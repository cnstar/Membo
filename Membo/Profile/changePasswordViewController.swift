//
//  changePasswordViewController.swift
//  Membo
//
//  Created by Ameya Vichare on 08/10/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit
import TextFieldEffects

class changePasswordViewController: UIViewController {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var passTable: UITableView!
    var placeholders = ["Old Password", "New Password", "Confirm New Password"]
    var passwords = [String](repeating: "", count: 3)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
        self.addSpacingToTitle(navBar, title: "CHANGE PASSWORD")
        setupTable()
    }
    
    func setupTable() {
        
        passTable.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 150))
        passTable.estimatedRowHeight = 50
        passTable.rowHeight = UITableViewAutomaticDimension
    }
    
    func handleTextfields(_ textField: HoshiTextField) {
        
        textField.placeholderColor = UIColorFromHex(rgbValue: 0x999999)
        textField.textColor = UIColor.black
        textField.borderInactiveColor = UIColorFromHex(rgbValue: 0x999999)
        textField.borderActiveColor = UIColorFromHex(rgbValue: 0x999999)
        textField.placeholderFontScale = 0.9
    }
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func changePressed(_ sender: Any) {
        
        if (passwords[0].isNotEmpty && passwords[1].isNotEmpty) && passwords[2].isNotEmpty {
            
            if passwords[0].characters.count >= 6 {
                
                if passwords[1].characters.count >= 6 {
                    
                    if passwords[1] == passwords[2] {
                        
                        changePassword()
                    }
                    else {
                        
                        self.Toast(text: "New Password and Confirm New Password don't match.")
                    }
                }
                else {
                    
                    self.Toast(text: "New Password length cannot be less than 6 characters.")
                }
            }
            else {
                
                self.Toast(text: "Old Password length cannot be less than 6 characters.")
            }
        }
        else {
            
            self.Toast(text: "Fill in all the details.")
        }
    }
    
    func changePassword() {
        
        self.postServer(param: ["userId": self.getUserId(), "oldPassword": passwords[0], "newPassword": passwords[1]], url: K.MEMBO_ENDPOINT + "changePassword") { [unowned self] (json) in
            
            if json["error"].stringValue == "false" {
                
                self.Toast(text: "Password changed.")
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                    
                    self.navigationController?.popViewController(animated: true)
                })
            }
            else {
                
                self.Toast(text: json["message"].stringValue)
            }
        }
    }
}

extension changePasswordViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "pass", for: indexPath) as! passwordTableViewCell
        
        cell.input.tag = indexPath.row
        cell.input.delegate = self
        cell.input.placeholder = placeholders[indexPath.row]
        handleTextfields(cell.input)
        
        cell.selectionStyle = .none
        return cell
    }
}

extension changePasswordViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if passwords.indices.contains(textField.tag) {
            
            passwords[textField.tag] = textField.text!
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
}
