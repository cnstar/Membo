//
//  profileViewController.swift
//  Membo
//
//  Created by Ameya Vichare on 24/09/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit

class profileViewController: UIViewController {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var hamButton: UIBarButtonItem!
    @IBOutlet weak var profileTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = true
        self.setupHam(navBar, button: hamButton)
        self.addSpacingToTitle(navBar, title: "MY PROFILE")
        setupTable()
        
//        let revealController: SWRevealViewController? = revealViewController()
//        revealViewController().delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        NotificationCenter.default.addObserver(self, selector: #selector(hamOpened(_:)), name: .hamOpen, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(hamClosed(_:)), name: .hamClose, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func hamOpened(_ notification: Notification) {
        
        self.view.isUserInteractionEnabled = false
        self.tabBarController?.tabBar.isUserInteractionEnabled = false
    }
    
    @objc func hamClosed(_ notification: Notification) {
        
        self.view.isUserInteractionEnabled = true
        self.tabBarController?.tabBar.isUserInteractionEnabled = true
    }
    
    func setupTable()  {
        
        profileTable.estimatedRowHeight = 40
        profileTable.rowHeight = UITableViewAutomaticDimension
    }
    
    @IBAction func notifPressed(_ sender: Any) {
        
        let homeTab = UIStoryboard(name: "HomeTab", bundle: nil)
        if let vc = homeTab.instantiateViewController(withIdentifier: "notifVC") as? notificationViewController {
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func chatPressed(_ sender: Any) {
        
        let homeTab = UIStoryboard(name: "HomeTab", bundle: nil)
        if let vc = homeTab.instantiateViewController(withIdentifier: "chatMainVC") as? chatMainViewController {
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension profileViewController: SWRevealViewControllerDelegate {
    
    func revealController(_ revealController: SWRevealViewController!, willMoveTo position: FrontViewPosition) {
        
        if position == .left {
            
            self.view.isUserInteractionEnabled = true
            self.tabBarController?.tabBar.isUserInteractionEnabled = true
        }
        else {
            
            self.view.isUserInteractionEnabled = false
            self.tabBarController?.tabBar.isUserInteractionEnabled = false
        }
    }
}

extension profileViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapped(_:)))
            let cell = tableView.dequeueReusableCell(withIdentifier: "profileHeader", for: indexPath) as! profileHeaderTableViewCell
            cell.name.text = self.getUsername().capitalized
            cell.location.text = self.getGender().capitalized
            makeItRound(cell.pic)
            cell.backImage.image = self.getUserImage()
            cell.pic.image = self.getUserImage()
            cell.pic.addGestureRecognizer(tap)
            cell.selectionStyle = .none
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "detail", for: indexPath) as! profileDetailTableViewCell
            cell.delegate = self
            cell.title.addTextSpacing(spacing: 1.08)
            cell.detail.addTextSpacing(spacing: 1.08)
            cell.changeButton.addTextSpacing(spacing: 1.08)
            
            if indexPath.row == 1 {
                
                cell.title.text = "Mobile No."
                cell.detail.text = self.getUserMobile()
                cell.changeButton.isHidden = true
            }
            else {
                
                cell.title.text = "Password"
                cell.detail.text = "***********"
                cell.changeButton.isHidden = false
            }
            
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tapped(_ gesture: UIGestureRecognizer) {
        
        let onboard = UIStoryboard(name: "Onboard", bundle: nil)
        if let vc = onboard.instantiateViewController(withIdentifier: "profilePicVC") as? profilePicViewController {
            
            vc.isEdit = true
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func makeItRound(_ view: UIView) {
        
        view.layer.cornerRadius = 65
        view.clipsToBounds = true
        view.layer.borderWidth = 2
        view.layer.borderColor = UIColorFromHex(rgbValue: 0xdc143c).cgColor
    }
}

extension profileViewController: imageChanged {
    
    func change() {
        
        self.profileTable.reloadData()
    }
}

extension profileViewController: changePass {
    
    func changeIt() {
        
        if let vc = storyboard?.instantiateViewController(withIdentifier: "changePass") as? changePasswordViewController {
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
