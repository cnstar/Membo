//
//  LiveVideo.swift
//  Membo
//
//  Created by Ameya Vichare on 07/12/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import Foundation

class liveVideo {
    
    var ls_id: String
    var stream_image: String
    var title: String
    var unique_id: String
    var is_live: String
    var start_date: String
    var end_date: String
    var imageHidden: Bool
    
    init(ls_id: String, stream_image: String, title: String, unique_id: String, is_live: String, start_date: String, end_date: String, imageHidden: Bool) {
        
        self.ls_id = ls_id
        self.stream_image = stream_image
        self.title = title
        self.unique_id = unique_id
        self.is_live = is_live
        self.start_date = start_date
        self.end_date = end_date
        self.imageHidden = imageHidden
    }
}
