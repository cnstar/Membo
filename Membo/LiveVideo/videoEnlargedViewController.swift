//
//  videoEnlargedViewController.swift
//  Membo
//
//  Created by Ameya Vichare on 07/12/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit

class videoEnlargedViewController: UIViewController {

    @IBOutlet weak var liveVideo: UIWebView!
    @IBOutlet weak var navBar: UINavigationBar!
    var uniqueId = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        liveVideo.scrollView.contentInset = UIEdgeInsets.zero
        liveVideo.loadHTMLString("<iframe width=\"\(liveVideo.frame.size.width - 20)\" height=\"\(liveVideo.frame.size.height - 20)\" src=\"https://www.youtube.com/embed/\(uniqueId)\" frameborder=\"0\" allowfullscreen></iframe>", baseURL: nil)
        
//        liveVideo.loadRequest(URLRequest(url: URL(string: "https://www.youtube.com/embed/\(uniqueId)")!))
    }
    
    override func viewWillLayoutSubviews() {
     
        super.viewWillLayoutSubviews()
        liveVideo.scrollView.contentInset = UIEdgeInsets.zero
    }
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
}
