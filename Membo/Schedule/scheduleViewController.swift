//
//  scheduleViewController.swift
//  Membo
//
//  Created by Ameya Vichare on 24/09/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit

class scheduleViewController: UIViewController {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var hamButton: UIBarButtonItem!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var day1: UITableView!
    @IBOutlet weak var day2: UITableView!
    @IBOutlet weak var day3: UITableView!
    @IBOutlet weak var dateCollection: UICollectionView!
    @IBOutlet weak var day4: UITableView!
    
    var isSwipe = Bool()
    var current = 3
    var currentDate = 0
    var day1Event = [Event]()
    var day2Event = [Event]()
    var day3Event = [Event]()
    var day4Event = [Event]()
    var headerDate = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = true
        self.setupHam(navBar, button: hamButton)
        self.addSpacingToTitle(navBar, title: "SCHEDULE")
        setUpScroll()
        setupTable(day1)
        setupTable(day2)
        setupTable(day3)
        setupTable(day4)
        getEvents()
        
//        let revealController: SWRevealViewController? = revealViewController()
//        revealViewController().delegate = self
    }
    
    func setupTable(_ table: UITableView) {

        table.tableFooterView = UIView(frame: CGRect.zero)
        table.estimatedRowHeight = 40
        table.rowHeight = UITableViewAutomaticDimension
    }
    
    func setUpScroll() {
        
        self.scrollView.frame = CGRect(x: 0, y: 144, width: self.view.frame.width, height: self.view.frame.height - 49 - 80 - 64)
        
        self.scrollView.contentSize = CGSize(width: self.scrollView.frame.width * 4, height: self.scrollView.frame.height)
        self.scrollView.delegate = self
        self.scrollView.isPagingEnabled = true
        
        self.day1.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: scrollView.frame.size.height)
        
        self.day2.frame = CGRect(x: self.view.frame.size.width, y: 0, width: self.view.frame.size.width, height: scrollView.frame.size.height)
        
        self.day3.frame = CGRect(x: self.view.frame.size.width * 2, y: 0, width: self.view.frame.size.width, height: scrollView.frame.size.height)
        self.day4.frame = CGRect(x: self.view.frame.size.width * 3, y: 0, width: self.view.frame.size.width, height: scrollView.frame.size.height)
    }
    
    @IBAction func notifPressed(_ sender: Any) {
        
        let homeTab = UIStoryboard(name: "HomeTab", bundle: nil)
        if let vc = homeTab.instantiateViewController(withIdentifier: "notifVC") as? notificationViewController {
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func chatPressed(_ sender: Any) {
        
        let homeTab = UIStoryboard(name: "HomeTab", bundle: nil)
        if let vc = homeTab.instantiateViewController(withIdentifier: "chatMainVC") as? chatMainViewController {
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func getEvents() {
        
        var count = 1
        var eve: Event?

        self.getServer(url: K.MEMBO_ENDPOINT + "getSchedule/\(self.getWeddingId())/\(self.getUserId())") { [unowned self] (json) in
            
            if json["error"].stringValue == "false" {
                
                print(json)
                
                for item in json["schedule"].arrayValue {
                    
                    self.headerDate.append(item["event_date"].stringValue)
                    
                    for innerItem in item["event_data"].arrayValue {
                        
                        eve = Event(event_id: innerItem["event_id"].stringValue, wedding_id: innerItem["wedding_id"].stringValue, event_name: innerItem["event_name"].stringValue, event_location: innerItem["event_location"].stringValue, event_desc: innerItem["event_desc"].stringValue, event_date_time: innerItem["event_date_time"].stringValue, event_date: innerItem["event_date"].stringValue, event_time: innerItem["event_time"].stringValue, date: innerItem["date"].stringValue)
                        
                        if count == 1 {
                            
                            self.day1Event.append(eve!)
                            DispatchQueue.main.async {
                                
                                self.day1.reloadData()
                            }
                        }
                        else if count == 2 {
                            
                            self.day2Event.append(eve!)
                            DispatchQueue.main.async {
                                
                                self.day2.reloadData()
                            }
                        }
                        else if count == 3 {
                            
                            self.day3Event.append(eve!)
                            DispatchQueue.main.async {
                                
                                self.day3.reloadData()
                            }
                        }
                        else if count == 4 {
                            
                            self.day4Event.append(eve!)
                            DispatchQueue.main.async {
                                
                                self.day4.reloadData()
                            }
                        }
                    }
                    
                    DispatchQueue.main.async {
                        
                        self.dateCollection.reloadData()
                    }
                
                    count += 1
                }
            }
        }
    }
}

extension scheduleViewController: SWRevealViewControllerDelegate {
    
    func revealController(_ revealController: SWRevealViewController!, willMoveTo position: FrontViewPosition) {
        
        if position == .left {
            
            self.view.isUserInteractionEnabled = true
            self.tabBarController?.tabBar.isUserInteractionEnabled = true
        }
        else {
            
            self.view.isUserInteractionEnabled = false
            self.tabBarController?.tabBar.isUserInteractionEnabled = false
        }
    }
}


extension scheduleViewController: UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        var frame: CGRect = self.scrollView.frame
        let currentLocation = scrollView.contentOffset.y
        frame.origin.x = frame.origin.x
        
        if (frame.origin.y - 144) > currentLocation{
            
        }else if (frame.origin.y - 144) < currentLocation{
            
        }
        else if frame.origin.x == scrollView.contentOffset.x && isSwipe {
            
            let pageWidth:CGFloat = scrollView.frame.width
            let currentPage:CGFloat = floor((scrollView.contentOffset.x-pageWidth/2)/pageWidth)+1
            
            print(pageWidth)
            print(currentPage)
            print("here")
            print(scrollView.contentOffset.x)
            
            if Int(currentPage) == 0{
                
                currentDate = 0
                DispatchQueue.main.async {
                    
                    self.dateCollection.reloadData()
                }
            }
            else if Int(currentPage) == 1{
                
                currentDate = 1
                DispatchQueue.main.async {
                    
                    self.dateCollection.reloadData()
                }
            }
        }
        else if frame.origin.x < scrollView.contentOffset.x && isSwipe {
            
            let pageWidth:CGFloat = scrollView.frame.width
            let currentPage:CGFloat = floor((scrollView.contentOffset.x-pageWidth/2)/pageWidth)+1

            if Int(currentPage) == 1{
                
                currentDate = 1
                DispatchQueue.main.async {
                    
                    self.dateCollection.reloadData()
                }
            }
            else if Int(currentPage) == 2{
                
                currentDate = 2
                DispatchQueue.main.async {
                    
                    self.dateCollection.reloadData()
                }
            }
            else if Int(currentPage) == 3{
                
                currentDate = 3
                DispatchQueue.main.async {
                    
                    self.dateCollection.reloadData()
                }
            }
        }
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        if targetContentOffset.pointee.y < scrollView.contentOffset.y {
            
            isSwipe = false
        }
        else if targetContentOffset.pointee.y > scrollView.contentOffset.y {
            
            isSwipe = false
        }
        else if targetContentOffset.pointee.x < scrollView.contentOffset.x {
            
            self.isSwipe = true
            let targetOffset = targetContentOffset.pointee.x
            let roundedOffset = round(targetOffset / scrollView.frame.width) * scrollView.frame.width
            targetContentOffset.pointee = CGPoint(x: roundedOffset, y: 0)
        }
        else if targetContentOffset.pointee.x > scrollView.contentOffset.x {
            
            self.isSwipe = true
            let targetOffset = targetContentOffset.pointee.x
            let roundedOffset = round(targetOffset / scrollView.frame.width) * scrollView.frame.width
            targetContentOffset.pointee = CGPoint(x: roundedOffset, y: 0)
        }
    }
}

extension scheduleViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == day1 {
            
            return day1Event.count
        }
        else if tableView == day2 {
            
            return day2Event.count
        }
        else if tableView == day3 {
            
            return day3Event.count
        }
        else {
            
            return day4Event.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var eve: Event
        
        if tableView == day1 {
            
            eve = day1Event[indexPath.row]
        }
        else if tableView == day2 {
            
            eve = day2Event[indexPath.row]
        }
        else if tableView == day3 {
            
            eve = day3Event[indexPath.row]
        }
        else {
            
            eve = day4Event[indexPath.row]
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "day", for: indexPath) as! scheduleTableViewCell
        
        cell.time.text = giveMeTime(eve.event_date_time, type: "time")
        cell.am.text = giveMeTime(eve.event_date_time, type: "am/pm")
        cell.title.text = eve.event_name
        cell.desc.text = eve.event_desc
        
        cell.time.addTextSpacing(spacing: 1.03)
        cell.am.addTextSpacing(spacing: 1.03)
        cell.title.addTextSpacing(spacing: 1.05)
        cell.desc.addTextSpacing(spacing: 1.05)
        
//        if indexPath.row == current {
//
//            cell.indicator.image = #imageLiteral(resourceName: "timeLineSelected")
//            cell.title.textColor = UIColorFromHex(rgbValue: 0xdc143c)
//            cell.title.font = UIFont(name: "OpenSans-SemiBold", size: 15)
//        }
//        else {
        
            cell.indicator.image = #imageLiteral(resourceName: "timeLineDeselected")
            cell.title.textColor = UIColor.black
            cell.title.font = UIFont(name: "OpenSans-Regular", size: 15)
//        }

        cell.selectionStyle = .none
        return cell
    }
    
    func giveMeTime(_ dateString: String, type: String) -> String {
        
        let formatter = DateFormatter()
        
        if type == "am/pm" {
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let date = formatter.date(from: dateString)
            formatter.dateFormat = "a"
            formatter.amSymbol = "AM"
            formatter.pmSymbol = "PM"
            return formatter.string(from: date!)
        }
        else if type == "time" {
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let date = formatter.date(from: dateString)
            formatter.dateFormat = "h:mm"
            formatter.amSymbol = "AM"
            formatter.pmSymbol = "PM"
            return formatter.string(from: date!)
        }
        else if type == "date" {
            formatter.dateFormat = "yyyy-MM-dd"
            let date = formatter.date(from: dateString)
            formatter.dateFormat = "dd"
            return formatter.string(from: date!)
        }
        else if type == "weekday" {
            formatter.dateFormat = "yyyy-MM-dd"
            let date = formatter.date(from: dateString)
            formatter.dateFormat = "EEE"
            return formatter.string(from: date!)
        }
        
        return ""
    }
}

extension scheduleViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return headerDate.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "date", for: indexPath) as! scheduleDateCollectionViewCell
        cell.dayOfWeek.text = giveMeTime(headerDate[indexPath.row], type: "weekday").uppercased()
        cell.date.text = giveMeTime(headerDate[indexPath.row], type: "date")
        
        if indexPath.item == currentDate {
            
            cell.indicator.isHidden = false
        }
        else {
            
            cell.indicator.isHidden = true
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        currentDate = indexPath.row
        DispatchQueue.main.async {
            
            self.dateCollection.reloadData()
        }
        
        let offset = CGPoint(x: self.view.frame.size.width * CGFloat(indexPath.row), y: 0)
        self.scrollView.setContentOffset(offset, animated: true)
    }
}
