//
//  scheduleDateCollectionViewCell.swift
//  Membo
//
//  Created by Ameya Vichare on 26/09/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit

class scheduleDateCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var dayOfWeek: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var indicator: UIImageView!
    
}
