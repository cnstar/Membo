//
//  scheduleTableViewCell.swift
//  Membo
//
//  Created by Ameya Vichare on 26/09/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit

class scheduleTableViewCell: UITableViewCell {

    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var am: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var desc: UILabel!
    @IBOutlet weak var indicator: UIImageView!
}
