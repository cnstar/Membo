//
//  Guest.swift
//  Membo
//
//  Created by Ameya Vichare on 04/10/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import Foundation

class Guest {
    
    var userId: String
    var guestName: String
    var mobileNo: String
    var email: String
    var profilePic: String
    var gender: String
    var location: String
    var guestFrom: String
    var guestRelation: String
    var countryCode: String
    var isVip: String
    var rsvpStatus: String
    
    init(userId: String, guestName: String, mobileNo: String, email: String, profilePic: String, gender: String, location: String, guestFrom: String, guestRelation: String, countryCode: String, isVip: String, rsvpStatus: String) {
        
        self.userId = userId
        self.guestName = guestName
        self.mobileNo = mobileNo
        self.email = email
        self.profilePic = profilePic
        self.gender = gender
        self.location = location
        self.guestFrom = guestFrom
        self.guestRelation = guestRelation
        self.countryCode = countryCode
        self.isVip = isVip
        self.rsvpStatus = rsvpStatus
    }
}
