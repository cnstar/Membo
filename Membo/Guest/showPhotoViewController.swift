//
//  showPhotoViewController.swift
//  Membo
//
//  Created by Ameya Vichare on 12/12/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit

class showPhotoViewController: UIViewController {

    @IBOutlet weak var img: UIImageView!
    var passedImage = UIImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        img.image = passedImage
        showAnimation()
    }
    
    @IBAction func backPressed(_ sender: Any) {
        
        removeAnimation()
    }
    
    func showAnimation() {
        
        //zoom in animation for popup
        self.view.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        self.view.alpha = 0
        UIView.animate(withDuration: 0.3) {
            self.view.alpha = 1
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }
    }
    
    func removeAnimation() {
        
        //zoom out for closing view
        self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        self.view.alpha = 1
        UIView.animate(withDuration: 0.3, animations: {
            self.view.alpha = 0
            self.view.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        }) { (result) in
            self.view.removeFromSuperview()
        }
    }
    
}
