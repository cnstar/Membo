//
//  guestViewController.swift
//  Membo
//
//  Created by Ameya Vichare on 24/09/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit

class guestViewController: UIViewController {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var hamButton: UIBarButtonItem!
    @IBOutlet weak var search: leftPaddedTextField!
    @IBOutlet weak var guestTable: UITableView!
    @IBOutlet weak var chatButton: UIBarButtonItem!
    @IBOutlet weak var notifButton: UIBarButtonItem!
    
    var limit = 0
    var guest = [Guest]()
    var filteredGuest = [Guest]()
    var isSearching = Bool()
    var isNewChat = Bool()
    var isAdm = Bool()
    var onlineUsers = [String]()
    
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchHeight: NSLayoutConstraint!
    
    var isWhoLiked = Bool()
    var feedId = String()
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        
        return refreshControl
    }()
    
    func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        getGuests()
    }
    
    deinit {
        
        print("guestViewController deinit")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = true
        self.hideKeyboardWhenTappedAround()
        
//        let revealController: SWRevealViewController? = revealViewController()
//        revealViewController().delegate = self

        if isNewChat {
            
            navBar.setBackgroundImage(UIImage(), for: .default)
            navBar.shadowImage = UIImage()
            navBar.isTranslucent = true
            
            hamButton.image = #imageLiteral(resourceName: "back")
            chatButton.image = nil
            chatButton.isEnabled = false
            notifButton.image = nil
            notifButton.isEnabled = false
            
            if isWhoLiked {
                
                getLikers()
                self.searchView.isHidden = true
                self.searchHeight.constant = 0
                self.addSpacingToTitle(navBar, title: "GUESTS WHO LIKED THIS")
            }
            else {
                
                guestTable.addSubview(refreshControl)
                setupSearch(search)
                self.addSpacingToTitle(navBar, title: "START CHATTING")
                getGuests()
            }
        }
        else {
            
            guestTable.addSubview(refreshControl)
            setupSearch(search)
            getGuests()
            self.addSpacingToTitle(navBar, title: "GUEST")
            
            if isAdm {
                
                hamButton.image = #imageLiteral(resourceName: "back")
            }
            else {
            
//                self.revealViewController().delegate = self
                self.setupHam(navBar, button: hamButton)
            }
        }

        setupTable()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadOnline(_:)), name: .online, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(hamOpened(_:)), name: .hamOpen, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(hamClosed(_:)), name: .hamClose, object: nil)
    }
    
    func reloadOnline(_ notification: Notification) {
        
        if let userId = notification.object as? [String] {
            
            onlineUsers = userId
            print(userId)
            changeOnlineUI()
        }
    }
    
    func checkStoredOnlineUsers() {
        
        if var users = UserDefaults.standard.value(forKey: "onlineUsers") as? [String] {
            
            for i in 0..<guest.count {
                
                for j in 0..<users.count {
                    
                    if users[j] == guest[i].userId {
                        
                        guest[i].isVip = "1"
                    }
                }
            }
            
            DispatchQueue.main.async {
                
                self.guestTable.reloadData()
            }
        }
    }
    
    func changeOnlineUI() {
        
        for i in 0..<guest.count {
            
            for j in 0..<onlineUsers.count {
                
                if onlineUsers[j] == guest[i].userId {
                    
                    guest[i].isVip = "1"
                }
                else {
                    
                    guest[i].isVip = "0"
                }
            }
        }
        
        DispatchQueue.main.async {
            
            self.guestTable.reloadData()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func hamOpened(_ notification: Notification) {
        
        self.view.isUserInteractionEnabled = false
        self.tabBarController?.tabBar.isUserInteractionEnabled = false
    }
    
    @objc func hamClosed(_ notification: Notification) {
        
        self.view.isUserInteractionEnabled = true
        self.tabBarController?.tabBar.isUserInteractionEnabled = true
    }
    
    @IBAction func hamPressed(_ sender: Any) {
        
        if isNewChat || isAdm {
            
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func setupTable() {

        guestTable.tableFooterView = UIView(frame: CGRect.zero)
        guestTable.estimatedRowHeight = 40
        guestTable.rowHeight = UITableViewAutomaticDimension
    }
    
    func setupSearch(_ textField: UITextField) {
        
        textField.layer.cornerRadius = 17.5
        textField.clipsToBounds = true
        textField.layer.borderWidth = 1
        textField.layer.borderColor = UIColorFromHex(rgbValue: 0x999999).cgColor
        textField.delegate = self
        textField.addTarget(self, action: #selector(self.textfieldChanged(_:)), for: .editingChanged)
        
        let searchImage = UIImageView(frame: CGRect(x: 13, y: 9, width: 17, height: 17))
        searchImage.image = #imageLiteral(resourceName: "searchBar")
        textField.addSubview(searchImage)
    }
    
    func getLikers() {
        
        guest.removeAll()
        
        DispatchQueue.main.async {
            
            self.guestTable.reloadData()
        }
        
        self.getServer(url: K.MEMBO_ENDPOINT + "getLikeListing/\(feedId)") { [unowned self] (json) in
            
            for item in json["likeUsers"].arrayValue {
                    
                self.guest.append(Guest(userId: item["userId"].stringValue, guestName: item["userName"].stringValue, mobileNo: item["mobile_no"].stringValue, email: "", profilePic: item["profilePic"].stringValue, gender: item["gender"].stringValue, location: item["location"].stringValue, guestFrom: "", guestRelation: "", countryCode: "", isVip: "", rsvpStatus: ""))
                    
                DispatchQueue.main.async {
                        
                    self.guestTable.reloadData()
                        
                }
            }
        }
    }
    
    func getGuests() {
        
        guest.removeAll()
        
        DispatchQueue.main.async {
            
            self.guestTable.reloadData()
        }
        
        self.getServer(url: K.MEMBO_ENDPOINT + "getGuestList/\(self.getWeddingId())/\(self.isAdmin())/\(self.getUserId())/\(limit)") { [unowned self] (json) in
            
            self.refreshControl.endRefreshing()
            
            for item in json["guestList"].arrayValue {
                    
                self.guest.append(Guest(userId: item["userId"].stringValue, guestName: item["guestName"].stringValue, mobileNo: item["mobileNo"].stringValue, email: item["email"].stringValue, profilePic: item["profilePic"].stringValue, gender: item["gender"].stringValue, location: item["location"].stringValue, guestFrom: item["guestFrom"].stringValue, guestRelation: item["guestRelation"].stringValue, countryCode: item["countryCode"].stringValue, isVip: "0", rsvpStatus: item["rsvpStatus"].stringValue))
                
                DispatchQueue.main.async {
                    
                    self.guestTable.reloadData()
                }
                
                self.checkStoredOnlineUsers()
            }
        }
    }
    
    @IBAction func notifPressed(_ sender: Any) {
        
        let homeTab = UIStoryboard(name: "HomeTab", bundle: nil)
        if let vc = homeTab.instantiateViewController(withIdentifier: "notifVC") as? notificationViewController {
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func chatPressed(_ sender: Any) {
        
        let homeTab = UIStoryboard(name: "HomeTab", bundle: nil)
        if let vc = homeTab.instantiateViewController(withIdentifier: "chatMainVC") as? chatMainViewController {
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView == guestTable {
            
            search.resignFirstResponder()
        }
    }
    
    func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {

        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        if maximumOffset - currentOffset <= 10.0 {
            
            if guest.count >= limit + 10 {
                limit += 10
                self.getGuests()
            }
        }
    }
}

extension guestViewController: SWRevealViewControllerDelegate {
    
    func revealController(_ revealController: SWRevealViewController!, willMoveTo position: FrontViewPosition) {
        
        if position == .left {
            
            self.view.isUserInteractionEnabled = true
            self.tabBarController?.tabBar.isUserInteractionEnabled = true
        }
        else {
            
            self.view.isUserInteractionEnabled = false
            self.tabBarController?.tabBar.isUserInteractionEnabled = false
        }
    }
}

extension guestViewController: UITextFieldDelegate {

    func textfieldChanged(_ textField: UITextField) {
        
        if (textField.text?.characters.count)! >= 3 {
            
            isSearching = true
            searchQuery(query: textField.text!)
        }
        else {
            
            isSearching = false
            DispatchQueue.main.async {
                
                self.guestTable.reloadData()
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        search.resignFirstResponder()
        return true
    }
    
    func searchQuery(query: String) {
        
        print(query)
        
        filteredGuest.removeAll()
        DispatchQueue.main.async {
            
            self.guestTable.reloadData()
        }
        
        self.getServer(url: K.MEMBO_ENDPOINT + "getSearchGuestList/\(self.getWeddingId())/\(self.isAdmin())/\(self.getUserId())/\(query)") { [unowned self] (json) in
            
            print(json)
            
            for item in json["guestList"].arrayValue {
                    
                self.filteredGuest.append(Guest(userId: item["userId"].stringValue, guestName: item["guestName"].stringValue, mobileNo: item["mobileNo"].stringValue, email: item["email"].stringValue, profilePic: item["profilePic"].stringValue, gender: item["gender"].stringValue, location: item["location"].stringValue, guestFrom: item["guestFrom"].stringValue, guestRelation: item["guestRelation"].stringValue, countryCode: item["countryCode"].stringValue, isVip: "0", rsvpStatus: item["rsvpStatus"].stringValue))
                
                DispatchQueue.main.async {
                    
                    self.guestTable.reloadData()
                }
            }
        }
    }
}

extension guestViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isSearching {
            
            return filteredGuest.count
        }
        else {
            
            return guest.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var ge: Guest
        
        if isSearching {
            
            ge = filteredGuest[indexPath.row]
        }
        else {
            
            ge = guest[indexPath.row]
        }

        let cell = tableView.dequeueReusableCell(withIdentifier: "guest", for: indexPath) as! chatMainTableViewCell
        
        makeItRound(cell.pic)
        
        cell.pic.tag = indexPath.row
        cell.pic.layer.borderWidth = 2
        cell.pic.sd_setImage(with: URL(string: ge.profilePic), placeholderImage: #imageLiteral(resourceName: "images"))
        cell.name.text = ge.guestName
        cell.location.text = ge.location
        
        if ge.isVip == "1" {
            
            cell.pic.layer.borderColor = UIColor.green.cgColor
        }
        else {
            
            cell.pic.layer.borderColor = UIColor.red.cgColor
        }
        
        if isNewChat {
            
            cell.status.isHidden = true
        }
        else {
            
            if ge.rsvpStatus == "0" {
                
                cell.status.text = "Pending"
            }
            else if ge.rsvpStatus == "1" {
                
                cell.status.text = "Attending"
            }
            else if ge.rsvpStatus == "2" {
                
                cell.status.text = "Not Attending"
            }
        }

        cell.selectionStyle = .none
        return cell
    }
    
    
    func makeItRound(_ view: UIView) {
        
        view.layer.cornerRadius = self.view.frame.size.width * 22.5/414
        view.clipsToBounds = true
    }
}

extension guestViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var ge: Guest
        if isSearching {
            
            if filteredGuest.indices.contains(indexPath.row) {
                
                ge = filteredGuest[indexPath.row]
                postSelect(ge: ge)
            }
        }
        else {
            
            if guest.indices.contains(indexPath.row) {
                
                ge = guest[indexPath.row]
                postSelect(ge: ge)
            }
        }
    }
    
    func postSelect(ge: Guest) {
        
        if isNewChat {
            
            if isWhoLiked {
                
                let prof = UIStoryboard(name: "ProfileTab", bundle: nil)
                if let vc = prof.instantiateViewController(withIdentifier: "guestProfileVC") as? guestProfileViewController {
                    
                    vc.guest = ge
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            else {
                
                let tab = UIStoryboard(name: "HomeTab", bundle: nil)
                if let vc = tab.instantiateViewController(withIdentifier: "chatInsideVC") as? chatInsideViewController {
                    
                    vc.friendId = ge.userId
                    vc.hisName = ge.guestName
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
        else {
            
            let prof = UIStoryboard(name: "ProfileTab", bundle: nil)
            if let vc = prof.instantiateViewController(withIdentifier: "guestProfileVC") as? guestProfileViewController {
                
                if self.isAdmin() == 1 {
                    
                    vc.isGuestAdmin = true
                    vc.isPlanner = true
                }
                
                vc.guest = ge
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
}
