//
//  User.swift
//  Membo
//
//  Created by Ameya Vichare on 06/11/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import Foundation

class User {
    
    var lat: String
    var long: String
    var userName: String
    var id: String
    var mobile: String
    var pic: String
    
    init(lat: String, long: String, userName: String, id:String, mobile:String, pic: String) {
        
        self.long = long
        self.lat = lat
        self.userName = userName
        self.id = id
        self.mobile = mobile
        self.pic = pic
    }
}
