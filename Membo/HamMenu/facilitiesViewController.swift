//
//  facilitiesViewController.swift
//  Membo
//
//  Created by Ameya Vichare on 25/09/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit

class facilitiesViewController: UIViewController {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var facilityTable: UITableView!
    @IBOutlet weak var hamButton: UIBarButtonItem!
    
    var facilities = ["Accomodations","Arrival Ride","Departure Ride"]
    var images = [#imageLiteral(resourceName: "hotelHam"),#imageLiteral(resourceName: "steering-wheel"),#imageLiteral(resourceName: "steering-wheel")]
    var isRSVP = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.addSpacingToTitle(navBar, title: "MY FACILITIES")
        setupTable()
        setUpHamburger()
    }
    
    func setUpHamburger() {
        
        let revealController: SWRevealViewController? = revealViewController()
//
        
        if isRSVP {
            
            hamButton.image = #imageLiteral(resourceName: "back")
        }
        else {
            
            let revealController: SWRevealViewController? = revealViewController()
            _ = revealController?.panGestureRecognizer()
            _ = revealController?.tapGestureRecognizer()
            revealViewController().delegate = self
            
            hamButton.target = revealViewController()
            hamButton.action = #selector(SWRevealViewController.revealToggle(_:))
        }
    }
    
    @IBAction func backPressed(_ sender: Any) {
        
        if isRSVP {
            
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func setupTable() {
        
        facilityTable.tableFooterView = UIView(frame: CGRect.zero)
        facilityTable.estimatedRowHeight = 40
        facilityTable.rowHeight = UITableViewAutomaticDimension
    }
}

extension facilitiesViewController: SWRevealViewControllerDelegate {
    
    func revealController(_ revealController: SWRevealViewController!, willMoveTo position: FrontViewPosition) {
        
        if position == .left {
            
            self.view.isUserInteractionEnabled = true
        }
        else {
            
            self.view.isUserInteractionEnabled = false
        }
    }
}

extension facilitiesViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return facilities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "facility", for: indexPath)
        cell.textLabel?.text = facilities[indexPath.row]
        cell.textLabel?.font = UIFont(name: "OpenSans-Regular", size: 20.7)
        cell.textLabel?.addTextSpacing(spacing: 1.1)
        cell.imageView?.image = images[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
}

extension facilitiesViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 0:
            if let vc = storyboard?.instantiateViewController(withIdentifier: "accomodationsVC") as? accomodationsViewController {
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
        case 1:
            let homeTab = UIStoryboard(name: "HomeTab", bundle: nil)
            if let vc = homeTab.instantiateViewController(withIdentifier: "locateRideVC") as? locateRideViewController {
                
                vc.isDeparture = false
                self.navigationController?.pushViewController(vc, animated: true)
            }
        case 2:
            let homeTab = UIStoryboard(name: "HomeTab", bundle: nil)
            if let vc = homeTab.instantiateViewController(withIdentifier: "locateRideVC") as? locateRideViewController {
                
                vc.isDeparture = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
        default:
            break
        }
    }
}
