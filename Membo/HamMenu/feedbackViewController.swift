//
//  feedbackViewController.swift
//  Membo
//
//  Created by Ameya Vichare on 07/11/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit
import DLRadioButton

class feedbackViewController: UIViewController {
    
    @IBOutlet weak var buttonView: UIView!
    
    @IBOutlet weak var navBar: UINavigationBar!

    @IBOutlet weak var textView: UITextView!
    
    @IBOutlet weak var placeholderLab: UILabel!
    
    @IBOutlet weak var sendButton: UIButton!
    
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var hamButton: UIBarButtonItem!
    
    var current = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initialSetup()
        setupRadioButtons()
//        self.setupHam(navBar, button: hamButton)
        self.addSpacingToTitle(navBar, title: "FEEDBACK")
        self.hideKeyboardWhenTappedAround()
        
        let revealController: SWRevealViewController? = revealViewController()
        _ = revealController?.panGestureRecognizer()
        _ = revealController?.tapGestureRecognizer()
        
        hamButton.target = revealViewController()
        hamButton.action = #selector(SWRevealViewController.revealToggle(_:))

        revealViewController().delegate = self
    }
    
    func initialSetup() {
        
        self.addFiller()
        buttonView.layer.cornerRadius = 2
        buttonView.clipsToBounds = true
        buttonView.layer.borderColor = UIColor.lightGray.cgColor
        buttonView.layer.borderWidth = 0.5
        
        textView.layer.cornerRadius = 2
        textView.clipsToBounds = true
        textView.layer.borderColor = UIColor.lightGray.cgColor
        textView.layer.borderWidth = 0.5
        textView.delegate = self
        
        sendButton.layer.cornerRadius = 20
        sendButton.clipsToBounds = true
    }
    
    @IBAction func sendPressed(_ sender: Any) {
        
        if textView.text.characters.count > 0 {
            
            self.postServer(param: ["user_id":self.getUserId(),"wedding_id":self.getWeddingId(),"is_admin":self.isAdmin(),"name":self.getUsername(),"message": textView.text], url: K.MEMBO_ENDPOINT + "submitFeedback", completion: { [unowned self] (json) in
                
                if json["error"].stringValue == "false" {
                    
                    self.textView.text.removeAll()
                    self.Toast(text: "Feedback submitted.")
                }
                else {
                    
                    self.Toast(text: "Something went wrong. Try again later.")
                }
            })
        }
        else {
            
            self.Toast(text: "Feedback cannot be empty.")
        }
    }
    
    func setupRadioButtons() {
        
        let frame = CGRect(x: 12, y: 14, width: 200, height: 20);
        let firstRadioButton = createRadioButton(frame: frame, title: "It's Great", color: UIColor.red)
        firstRadioButton.isSelected = true
        firstRadioButton.tag = 1
        let frame2 = CGRect(x: 12, y: 50, width: 200, height: 20);
        let secondRadioButton = createRadioButton(frame: frame2, title: "It's Good", color: UIColor.red)
        secondRadioButton.tag = 2
        let frame3 = CGRect(x: 12, y: 86, width: 200, height: 20);
        let thirdRadioButton = createRadioButton(frame: frame3, title: "It's Ok", color: UIColor.red)
        thirdRadioButton.tag = 3
        let otherButtons = [secondRadioButton,thirdRadioButton]
        
        firstRadioButton.otherButtons = otherButtons
    }
    
    private func createRadioButton(frame : CGRect, title : String, color : UIColor) -> DLRadioButton {
        let radioButton = DLRadioButton(frame: frame);
        radioButton.titleLabel!.font = UIFont.systemFont(ofSize: 14);
        radioButton.setTitle(title, for: []);
        radioButton.setTitleColor(UIColor.black, for: []);
        radioButton.iconSize = 20.0
        radioButton.indicatorSize = 10.0
        radioButton.marginWidth = 18.0
        radioButton.iconColor = color;
        radioButton.indicatorColor = color;
        radioButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left;
        radioButton.addTarget(self, action: #selector(self.logSelectedButton), for: UIControlEvents.touchUpInside);
        self.buttonView.addSubview(radioButton);
        
        return radioButton;
    }
    
    func logSelectedButton(radioButton : DLRadioButton) {
        if (radioButton.isMultipleSelectionEnabled) {
            for button in radioButton.selectedButtons() {
                print(String(format: "%@ is selected.\n", button.titleLabel!.text!));
            }
        } else {
            
            current = radioButton.tag
//            print(String(format: "%@ is selected.\n", radioButton.selected()!.titleLabel!.text!));
        }
    }
}

extension feedbackViewController: SWRevealViewControllerDelegate {
    
    func revealController(_ revealController: SWRevealViewController!, willMoveTo position: FrontViewPosition) {
        
        if position == .left {
            
            self.view.isUserInteractionEnabled = true
        }
        else {
            
            self.view.isUserInteractionEnabled = false
        }
    }
}

extension feedbackViewController: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        
        if textView.text.characters.count > 0 {
            
            placeholderLab.isHidden = true
        }
        else {
            
            placeholderLab.isHidden = false
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        topConstraint.constant = -150
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        topConstraint.constant = 11
    }
}
