//
//  reqTableViewCell.swift
//  Membo
//
//  Created by Ameya Vichare on 12/12/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit

class reqTableViewCell: UITableViewCell {

    @IBOutlet weak var pic: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var gender: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var time: UILabel!
    
}
