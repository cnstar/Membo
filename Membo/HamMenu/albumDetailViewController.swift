//
//  albumDetailViewController.swift
//  Membo
//
//  Created by Ameya Vichare on 25/09/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit
import SDWebImage

class albumDetailViewController: UIViewController {
    
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var photoView: UICollectionView!
    
    var detail = String()
    var albumId = String()
    var album = [Album]()
    
    @IBOutlet weak var addPhotoButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.addSpacingToTitle(navBar, title: detail)
            
        addPhotoButton.isHidden = false
        addPhotoButton.layer.cornerRadius = 24
        addPhotoButton.clipsToBounds = true
    }
    
    @IBAction func addPhotoPressed(_ sender: Any) {
        
        if let vc = storyboard?.instantiateViewController(withIdentifier: "uploadVC") as? uploadViewController {
            
            vc.titlee = detail
            vc.isOldAlbum = true
            vc.albumId = albumId
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        getImages()
    }
    
    func getImages() {
        
        album.removeAll()
        
        DispatchQueue.main.async {
            
            self.photoView.reloadData()
        }
        
        self.getServer(url: K.MEMBO_ENDPOINT + "getGalleryImages/\(albumId)") { [unowned self] (json) in

            for item in json["gallery"].arrayValue {
                    
                self.album.append(Album(album_id: "", admin_id: "", album_name: item["image_title"].stringValue, album_image: item["filename"].stringValue))
                
                DispatchQueue.main.async {
                    
                    self.photoView.reloadData()
                }
            }
        }
    }
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
}

extension albumDetailViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return album.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var alb: Album
        alb = album[indexPath.row]
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "pic", for: indexPath) as! photoCollectionViewCell
        cell.pic.sd_setImage(with: URL(string: alb.album_image), placeholderImage: #imageLiteral(resourceName: "images"))
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let constant = self.view.frame.size.width * 107.8333/325
        
        return CGSize(width: constant, height: constant)
    }
}

extension albumDetailViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let vc = storyboard?.instantiateViewController(withIdentifier: "photoSlider") as? photoSliderViewController {
            
            vc.photoArray = album
            vc.index = indexPath.row
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
