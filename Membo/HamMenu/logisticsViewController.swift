//
//  logisticsViewController.swift
//  Membo
//
//  Created by Ameya Vichare on 12/12/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit

class logisticsViewController: UIViewController {
    
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var hamButton: UIBarButtonItem!

    @IBOutlet weak var guestTable: UITableView!
    @IBOutlet weak var chatButton: UIBarButtonItem!
    @IBOutlet weak var notifButton: UIBarButtonItem!

    var guest = [Guest]()
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        
        return refreshControl
    }()
    
    func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        getGuests()
    }
    
    deinit {
        
        print("guestViewController deinit")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = true
        self.hideKeyboardWhenTappedAround()
        let revealController: SWRevealViewController? = revealViewController()
        revealViewController().delegate = self
        
        guestTable.addSubview(refreshControl)
        getGuests()
        self.setupHam(navBar, button: hamButton)
        self.addSpacingToTitle(navBar, title: "LOGISTIC")
        setupTable()
    }
    
    func setupTable() {
        
        guestTable.tableFooterView = UIView(frame: CGRect.zero)
        guestTable.estimatedRowHeight = 40
        guestTable.rowHeight = UITableViewAutomaticDimension
    }
    
    func getGuests() {
        
        guest.removeAll()
        
        DispatchQueue.main.async {
            
            self.guestTable.reloadData()
        }
        
        self.getServer(url: K.MEMBO_ENDPOINT + "getLogisticDriverList/\(self.getUserId())/\(self.getWeddingId())") { [unowned self] (json) in
            
            print(json)
            
            self.refreshControl.endRefreshing()
            
            for item in json["drivers"].arrayValue {

                self.guest.append(Guest(userId: item["user_id"].stringValue, guestName: item["driverName"].stringValue, mobileNo: item["mobileNumber"].stringValue, email: item["email"].stringValue, profilePic: item["profilePic"].stringValue, gender: item["gender"].stringValue, location: item["carName"].stringValue + " " + item["carNumber"].stringValue, guestFrom: item["guestFrom"].stringValue, guestRelation: item["guestRelation"].stringValue, countryCode: item["countryCode"].stringValue, isVip: item["isVip"].stringValue, rsvpStatus: item["rsvpStatus"].stringValue))

                DispatchQueue.main.async {

                    self.guestTable.reloadData()
                }
            }
        }
    }
    
    @IBAction func notifPressed(_ sender: Any) {
        
        let homeTab = UIStoryboard(name: "HomeTab", bundle: nil)
        if let vc = homeTab.instantiateViewController(withIdentifier: "notifVC") as? notificationViewController {
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func chatPressed(_ sender: Any) {
        
        let homeTab = UIStoryboard(name: "HomeTab", bundle: nil)
        if let vc = homeTab.instantiateViewController(withIdentifier: "chatMainVC") as? chatMainViewController {
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension logisticsViewController: SWRevealViewControllerDelegate {
    
    func revealController(_ revealController: SWRevealViewController!, willMoveTo position: FrontViewPosition) {
        
        if position == .left {
            
            self.view.isUserInteractionEnabled = true
            self.tabBarController?.tabBar.isUserInteractionEnabled = true
        }
        else {
            
            self.view.isUserInteractionEnabled = false
            self.tabBarController?.tabBar.isUserInteractionEnabled = false
        }
    }
}

extension logisticsViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

            
        return guest.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var ge: Guest
            
        ge = guest[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "guest", for: indexPath) as! chatMainTableViewCell
        
        makeItRound(cell.pic)
        cell.pic.sd_setImage(with: URL(string: ge.profilePic), placeholderImage: #imageLiteral(resourceName: "images"))
        cell.name.text = ge.guestName
        cell.location.text = ge.location
        
        cell.selectionStyle = .none
        return cell
    }
    
    func makeItRound(_ view: UIView) {
        
        view.layer.cornerRadius = self.view.frame.size.width * 22.5/414
        view.clipsToBounds = true
    }
}

extension logisticsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "reqVC") as? requestsViewController {
            
            vc.driver_id = guest[indexPath.row].userId
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
