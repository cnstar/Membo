//
//  requestsViewController.swift
//  Membo
//
//  Created by Ameya Vichare on 12/12/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit

class requestsViewController: UIViewController {

    @IBOutlet weak var reqTable: UITableView!
    var driver_id = String()
    var guest = [Guest]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.addFiller()
        setupTable()
        getRequests()
    }
    
    func setupTable() {
        
        reqTable.estimatedRowHeight = 200
        reqTable.rowHeight = UITableViewAutomaticDimension
    }
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func getRequests() {
        
        self.getServer(url: K.MEMBO_ENDPOINT + "getRequestList/\(self.getWeddingId())") { [unowned self] (json) in
            
            print(json)
//
            for item in json["request"].arrayValue {

                self.guest.append(Guest(userId: item["pdId"].stringValue, guestName: item["username"].stringValue, mobileNo: item["mobileNo"].stringValue, email: item["pickupDropLocation"].stringValue, profilePic: item["profilePic"].stringValue, gender: item["gender"].stringValue, location: item["time"].stringValue, guestFrom: item["date"].stringValue, guestRelation: item["guestRelation"].stringValue, countryCode: item["countryCode"].stringValue, isVip: item["isVip"].stringValue, rsvpStatus: item["rsvpStatus"].stringValue))

                DispatchQueue.main.async {

                    self.reqTable.reloadData()
                }
            }
        }
    }
}

extension requestsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return guest.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var ge: Guest
        ge = guest[indexPath.row]

        let cell = tableView.dequeueReusableCell(withIdentifier: "req", for: indexPath) as! reqTableViewCell
        
        makeItRound(cell.pic)
        cell.pic.sd_setImage(with: URL(string: ge.profilePic), placeholderImage: #imageLiteral(resourceName: "images"))
        cell.name.text = ge.guestName
        cell.gender.text = ge.gender
        cell.location.text = ge.email
        cell.date.text = ge.guestFrom
        cell.time.text = ge.location
        
        cell.selectionStyle = .none
        return cell
    }
    
    func makeItRound(_ view: UIView) {
        
        view.layer.cornerRadius = 22.5
        view.clipsToBounds = true
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let alert = UIAlertController(title: "Do you want to assign driver?", message: "", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { [unowned self] action in

            self.postServer(param: ["driverId": self.driver_id, "pdId": self.guest[indexPath.row].userId], url: K.MEMBO_ENDPOINT + "assignDriver") { [unowned self] (json) in
                
                if json["error"].stringValue == "false" {
                    
                    self.Toast(text: "Driver assigned.")
                }
                else {
                    
                    self.Toast(text: "Something went wrong. Try again later.")
                }
            }
        }))
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
