//
//  qrViewController.swift
//  Membo
//
//  Created by Ameya Vichare on 25/09/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit

class qrViewController: UIViewController {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var hamButton: UIBarButtonItem!
    @IBOutlet weak var qrCodeImageView: UIImageView!
    
    var qrcodeImage: CIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.addSpacingToTitle(navBar, title: "QR - CODE")
        setUpHamburger()
        generateQRCode()
    }
    
    func generateQRCode() {
        
        let data = "\(self.getUserId()),\(self.getWeddingId())".data(using: String.Encoding.isoLatin1, allowLossyConversion: false)
        let filter = CIFilter(name: "CIQRCodeGenerator")
        
        filter?.setValue(data, forKey: "inputMessage")
        filter?.setValue("Q", forKey: "inputCorrectionLevel")
        qrcodeImage = filter?.outputImage
        
        let scaleX = qrCodeImageView.frame.size.width / qrcodeImage.extent.size.width
        let scaleY = qrCodeImageView.frame.size.height / qrcodeImage.extent.size.height
        let transformedImage = qrcodeImage.applying(CGAffineTransform(scaleX: scaleX, y: scaleY))
        
        qrCodeImageView.image = UIImage(ciImage: transformedImage)
    }
    
    func setUpHamburger() {
        
        let revealController: SWRevealViewController? = revealViewController()
        _ = revealController?.panGestureRecognizer()
        _ = revealController?.tapGestureRecognizer()
        
        hamButton.target = revealViewController()
        hamButton.action = #selector(SWRevealViewController.revealToggle(_:))
    }
}
