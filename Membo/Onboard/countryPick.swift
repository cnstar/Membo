//
//  countryPick.swift
//  Lets Partii
//
//  Created by Ameya Vichare on 17/09/17.
//  Copyright © 2017 vit. All rights reserved.
//

import Foundation

class CountryPick {
    
    var name: String
    var code: String
    var imageCode: String
    var image: UIImage

    init(name: String, code: String,imageCode: String,image:UIImage) {
        self.name = name
        self.code = code
        self.image = image
        self.imageCode = imageCode
    }
}

