//
//  Album.swift
//  Membo
//
//  Created by Ameya Vichare on 08/10/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import Foundation

class Album {
    
    var album_id: String
    var admin_id: String
    var album_name: String
    var album_image: String
    
    init(album_id: String, admin_id: String, album_name: String, album_image: String) {
        
        self.album_id = album_id
        self.admin_id = admin_id
        self.album_name = album_name
        self.album_image = album_image
    }
}
