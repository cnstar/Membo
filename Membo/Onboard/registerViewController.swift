//
//  registerViewController.swift
//  Membo
//
//  Created by Ameya Vichare on 23/09/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit
import TextFieldEffects
import DLRadioButton
import Firebase

class registerViewController: UIViewController {

    @IBOutlet weak var name: HoshiTextField!
    @IBOutlet weak var mobile: HoshiTextField!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var password: HoshiTextField!
    @IBOutlet weak var confirmPassword: HoshiTextField!
    @IBOutlet weak var maleButton: DLRadioButton!
    @IBOutlet weak var femaleButton: DLRadioButton!
    @IBOutlet weak var countryCodeView: UIView!
    @IBOutlet weak var countryIcon: UIImageView!
    @IBOutlet weak var lab: UILabel!
    
    var gender = "Male"
    var friendId = String()
    var weddingId = String()
    var countryName = String()
    var countryCode = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        hidePreviousNavBar()
        
        handleKeyBoard()
        
        handleTextfields()
        
        cutomizeLoginButton()
        
        hideNavBar()
        
        addTap()
    }
    
    func hideNavBar() {
        
        navBar.frame.size.height = 64
        navBar.setBackgroundImage(UIImage(), for: .default)
        navBar.shadowImage = UIImage()
        navBar.isTranslucent = true
    }
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func hidePreviousNavBar() {
        
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func handleKeyBoard() {
        
        self.hideKeyboardWhenTappedAround()
    }
    
    func addTap() {
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.countryTapped))
        countryCodeView.addGestureRecognizer(tap)
        
    }
    
    @objc func countryTapped() {
        
        print("tapped")
        
        if let popOverVC = storyboard?.instantiateViewController(withIdentifier: "codeVC") as? countryCodePopViewController {
            self.addChildViewController(popOverVC)
            popOverVC.delegate = self
            popOverVC.view.frame = self.view.frame
            self.view.addSubview(popOverVC.view)
            self.didMove(toParentViewController: self)
        }
    }
    
    func handleTextfields() {
        
        maleButton.isSelected = true
        
        name.placeholderColor = UIColorFromHex(rgbValue: 0x999999)
        name.textColor = UIColor.black
        name.borderInactiveColor = UIColorFromHex(rgbValue: 0x999999)
        name.borderActiveColor = UIColorFromHex(rgbValue: 0x999999)
        name.placeholderFontScale = 0.9
        name.tag = 0
        
        mobile.placeholderColor = UIColorFromHex(rgbValue: 0x999999)
        mobile.textColor = UIColor.black
        mobile.borderInactiveColor = UIColorFromHex(rgbValue: 0x999999)
        mobile.borderActiveColor = UIColorFromHex(rgbValue: 0x999999)
        mobile.placeholderFontScale = 0.9
        mobile.tag = 1
        
        password.placeholderColor = UIColorFromHex(rgbValue: 0x999999)
        password.textColor = UIColor.black
        password.borderInactiveColor = UIColorFromHex(rgbValue: 0x999999)
        password.borderActiveColor = UIColorFromHex(rgbValue: 0x999999)
        password.placeholderFontScale = 0.9
        password.tag = 2
        
        confirmPassword.placeholderColor = UIColorFromHex(rgbValue: 0x999999)
        confirmPassword.textColor = UIColor.black
        confirmPassword.borderInactiveColor = UIColorFromHex(rgbValue: 0x999999)
        confirmPassword.borderActiveColor = UIColorFromHex(rgbValue: 0x999999)
        confirmPassword.placeholderFontScale = 0.9
        confirmPassword.tag = 3
    }
    
    
    func cutomizeLoginButton() {
        
        nextButton.layer.cornerRadius = 26
        nextButton.clipsToBounds = true
    }
    
    @IBAction func malePressed(_ sender: Any) {
        
        gender = "Male"
    }
    
    @IBAction func femalePressed(_ sender: Any) {
        
        gender = "Female"
    }
    
    @IBAction func nextPressed(_ sender: Any) {
        
        if (name.text?.isNotEmpty)! {
            
            if (mobile.text?.isNotEmpty)! {
                
                if (password.text?.isNotEmpty)! {
                    
                    if (password.text?.characters.count)! >= 6 {
                        
                        if password.text == confirmPassword.text {
                            
                            signUp()
                        }
                        else {
                            
                            self.Toast(text: "Passwords don't match. Try again.")
                        }
                    }
                    else {
                        
                        self.Toast(text: "Password needs to be atleast 6 characters long.")
                    }
                }
                else {
                    
                    self.Toast(text: "Password can't be empty.")
                }
            }
            else {
                
                self.Toast(text: "Mobile can't be empty.")
            }
        }
        else {
            
            self.Toast(text: "Name can't be empty.")
        }
    }
    
    func signUp() {
        
        self.postServer(param: ["userName": name.text!, "password": password.text!, "gender": gender, "mobile": mobile.text!, "friendId": self.friendId, "weddingId": self.weddingId, "platform": "ios", "cc": countryCode, "cn": countryName], url: K.MEMBO_ENDPOINT + "register") { [unowned self] (json) in
            
            if json["error"].stringValue == "false" {
                
                UserDefaults.standard.set(json["user"]["weddingCode"].stringValue, forKey: "wedding_code")
                UserDefaults.standard.set(self.weddingId, forKey: "wedding_id")
                UserDefaults.standard.set(json["user"]["userId"].stringValue, forKey: "user_id")
                UserDefaults.standard.set(json["user"]["userName"].stringValue, forKey: "user_name")
                UserDefaults.standard.set(json["user"]["apiKey"].stringValue, forKey: "user_api")
                UserDefaults.standard.set(json["user"]["mobileNo"].stringValue, forKey: "user_mobile")
                UserDefaults.standard.set(true, forKey: "isLoggedIn")
                UserDefaults.standard.set(json["user"]["gender"].stringValue, forKey: "user_gender")
                UserDefaults.standard.set(json["user"]["rsvpStatus"].stringValue, forKey: "rsvpStatus")
                UserDefaults.standard.set(json["user"]["profilePic"].stringValue, forKey: "user_image_url")
                
                if let onlineUpdated = UserDefaults.standard.value(forKey: "onlineUpdated") as? Bool {
                    
                    if !onlineUpdated {
                        
                        UserDefaults.standard.set(true, forKey: "onlineUpdated")
                        SocketIOManager.sharedInstance.establishConnectionForRealTimeStatus(id: json["user"]["userId"].stringValue)
                    }
                }
                
                let window = UIApplication.shared.keyWindow!
                
                if let token = InstanceID.instanceID().token() {
                    
//                    window.makeToast("Uploading Token")
                    print("Uploading token")
                    
                    UserDefaults.standard.set(true, forKey: "tokenUploaded")
                    self.postServer2(param: ["userId": json["user"]["userId"].stringValue, "token": token, "platform": "ios"], url: K.MEMBO_ENDPOINT + "userToken", completion: { (json) in
                        
                        print("token: \(token)")
                        print(json)
                        
                        if json["error"].stringValue == "false" {
                            
                            
                        }
                    })
                }
                else {
                    
                    UserDefaults.standard.set(false, forKey: "tokenUploaded")
//                    window.makeToast("Token not found.")
                }
                
                if json["user"]["isAdmin"].stringValue == "0" {
                    
                    //normal user
                    UserDefaults.standard.set(false, forKey: "isAdmin")
                }
                else if json["user"]["isAdmin"].stringValue == "1" {
                    
                    //admin user
                    UserDefaults.standard.set(true, forKey: "isAdmin")
                }
                
                if json["user"]["loginStatus"].stringValue == "1" {
                    
                    //not first time login
                    
                    UserDefaults.standard.set(true, forKey: "isFirstTimeLoginDone")
                    
                    if json["user"]["isAdmin"].stringValue == "0" {
                        
                        UserDefaults.standard.set(0, forKey: "admin_identifier")
                        let tab = UIStoryboard(name: "TabBar", bundle: nil)
                        
                        if let vc = tab.instantiateViewController(withIdentifier: "revealVC") as? SWRevealViewController {
                            
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                    else if json["user"]["isAdmin"].stringValue == "1" {
                        
                        UserDefaults.standard.set(1, forKey: "admin_identifier")
                        let homeTab = UIStoryboard(name: "TabBar", bundle: nil)
                        
                        if let vc = homeTab.instantiateViewController(withIdentifier: "revealVC") as? SWRevealViewController {
                            
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                    else {
                        
                        let homeTab = UIStoryboard(name: "TabBar", bundle: nil)
                        
                        if let vc = homeTab.instantiateViewController(withIdentifier: "revealLogistics") as? SWRevealViewController {
                            
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                        
                        UserDefaults.standard.set(3, forKey: "admin_identifier")
                    }
                    
                    ImageLoader.sharedLoader.imageForUrl(urlString: json["user"]["profilePic"].stringValue, completionHandler: { (image: UIImage?, url: String) in
                        
                        let imageData = UIImagePNGRepresentation(image!)
                        UserDefaults.standard.set(imageData!, forKey: "user_image") //image pic
                    })
                }
                else if json["user"]["loginStatus"].stringValue == "0" {
                    
                    //first time login
                    UserDefaults.standard.set(false, forKey: "isFirstTimeLoginDone")
                    
                    if let vc = self.storyboard?.instantiateViewController(withIdentifier: "profilePicVC") as? profilePicViewController {
                        
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }
            else {
                
                self.Toast(text: json["message"].stringValue)
            }
        }
    }
}

extension registerViewController: countryBackPass {
    
    func passCountry(name: String,code: String, icon: UIImage) {
        
        countryIcon.image = icon
        lab.text = code
        countryName = name
        countryCode = code
    }
}

extension registerViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == name {
            
            mobile.becomeFirstResponder()
        }
        else {
            
            mobile.resignFirstResponder()
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.tag == 2 || textField.tag == 3 {
            
            topConstraint.constant = CGFloat(-20 * textField.tag)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        topConstraint.constant = 50
    }
}
