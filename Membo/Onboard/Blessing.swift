//
//  Blessing.swift
//  Membo
//
//  Created by Ameya Vichare on 08/10/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import Foundation

class Blessing {
    
    var username: String
    var message: String
    var blessingsText: String
    var timeElapsed: String
    var profilePic: String
    var blessingsTemplate: String
    
    init(username: String, message: String, blessingsText: String, timeElapsed: String, profilePic: String, blessingsTemplate: String) {
        
        self.username = username
        self.message = message
        self.blessingsText = blessingsText
        self.timeElapsed = timeElapsed
        self.profilePic = profilePic
        self.blessingsTemplate = blessingsTemplate
    }
}
