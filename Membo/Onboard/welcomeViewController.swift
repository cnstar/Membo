//
//  welcomeViewController.swift
//  Membo
//
//  Created by Ameya Vichare on 23/09/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class welcomeViewController: UIViewController {

    @IBOutlet weak var welcomeVideo: UIView!
    var player: AVPlayer?
    var avpController = AVPlayerViewController()
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var welcomeLab: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.isHidden = true
        videoPlayer()
        makeNextRound()
//        addShadow(welcomeLab)
//        addShadow(detailLabel)
    }
    
    @IBAction func nextPressed(_ sender: Any) {
        
        player?.pause()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.playerItemDidReachEnd(notification:)),name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,object: player?.currentItem)
    }
    
    func makeNextRound() {
        
        nextButton.layer.cornerRadius = 26
        nextButton.clipsToBounds = true
    }
    
    func addShadow(_ label: UILabel) {
        
        label.layer.shadowColor = UIColor.black.cgColor
        label.layer.shadowOffset = CGSize(width: 0, height: 0)
        label.layer.shadowOpacity = 1
        label.layer.shadowRadius = 1
    }
    
    func videoPlayer() {
        
        let moviePath = Bundle.main.path(forResource: "wedding", ofType: "mp4")

        if let path = moviePath{

//            let url = NSURL.fileURL(withPath: path)
//            let item = AVPlayerItem(url: url)
//            self.player = AVPlayer(playerItem: item)
//            self.avpController = AVPlayerViewController()
//            self.avpController.player = self.player
//            avpController.showsPlaybackControls = false
//            avpController.view.frame = welcomeVideo.frame
//            self.addChildViewController(avpController)
//            self.view.addSubview(avpController.view)
            
            player = AVPlayer(url: URL(fileURLWithPath: path))
            var playerLayer: AVPlayerLayer?
            playerLayer = AVPlayerLayer(player: player)
            playerLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
            playerLayer!.frame = self.welcomeVideo.frame
            self.welcomeVideo!.layer.addSublayer(playerLayer!)
            player?.play()
            
            self.view.bringSubview(toFront: nextButton)
            self.view.bringSubview(toFront: detailLabel)
            self.view.bringSubview(toFront: welcomeLab)
        }
        
        
    }
    
    func playerItemDidReachEnd(notification: Notification) {
        self.player?.seek(to: kCMTimeZero)
        self.player?.play()
    }
}
