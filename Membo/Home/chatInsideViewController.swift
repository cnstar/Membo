//
//  chatInsideViewController.swift
//  Membo
//
//  Created by Ameya Vichare on 25/09/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit
import JSQMessagesViewController
import SwiftyJSON
import SocketIO
import SDWebImage

var messages = [JSQMessage]()
var outgoingBubbleImageView: JSQMessagesBubbleImage!
var incomingBubbleImageView: JSQMessagesBubbleImage!

protocol updateChat: class {
    
    func updateIt()
}

extension Notification.Name {
    static let newMessage = Notification.Name("newMessage")
}

class chatInsideViewController: JSQMessagesViewController {
    
    var friendId = String()
    var chatroomId = String()
    var hisName = String()
    var hisImage: UIImage?
    var socket : SocketIOClient?
    weak var delegate: updateChat?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationBarSetup()
        initialSetup()
        setupBubbles()
        messages.removeAll()
        createChatroom()
    }
    
    override func viewDidLayoutSubviews() {
        
        self.scrollToBottom(animated: false)
    }
    
    func createChatroom() {
        
        UserDefaults.standard.set(friendId, forKey: "chatIdOfOtherPerson")
        
        if let friendId = UserDefaults.standard.value(forKey: "chatIdOfOtherPerson") as? String {
            
            print("inside")
            print(friendId)
        }
        
        print("creating chatroom ----------------")
        print(friendId)
        print(self.getUserId())
        
        self.postServer2(param: ["userId":self.getUserId(), "friendId": friendId], url: K.MEMBO_ENDPOINT + "createChatRoom") { [unowned self] (json) in

            print(json)

            if json["error"].stringValue == "false" {

                self.establishConnection(id: json["uc_id"].stringValue, username: self.getUsername())
                self.getChats(id: json["uc_id"].stringValue)
                self.chatroomId = json["uc_id"].stringValue
            }
        }
    }
    
    func getChats(id: String) {
        
        self.getServer3(url: K.MEMBO_ENDPOINT + "/getChats/\(id)") { [unowned self] (json) in

            if json["error"].stringValue == "false" {
                
                for item in json["chat_data"].arrayValue {

//                    if self.hisImage == nil {
//
//                        if item["user_id"].stringValue != "\(self.getUserId())" {
//
//                            ImageLoader.sharedLoader.imageForUrl(urlString: item["profile_pic"].stringValue, completionHandler: { [unowned self] (image: UIImage?, url: String) in
//
//                                self.hisImage = image!
//
//                                DispatchQueue.main.async {
//
//                                    self.finishSendingMessage()
//                                }
//                            })
//                        }
//                    }
                    
                    let msg = JSQMessage(senderId: item["user_id"].stringValue, displayName: item["user_name"].stringValue, text: item["message"].stringValue)
                    messages.append(msg!)
                    DispatchQueue.main.async {
                        
                        self.finishSendingMessage()
                    }
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        NotificationCenter.default.addObserver(self, selector: #selector(newMessage(_:)), name: .newMessage, object: nil)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        UserDefaults.standard.set("0", forKey: "chatIdOfOtherPerson")
        NotificationCenter.default.removeObserver(self)
    }
    
    func backPressed() {
        
        self.closeConnection()
        self.delegate?.updateIt()
        self.navigationController?.popViewController(animated: true)
    }
}

///////////////////////
// SETUP OTHER
///////////////////////

extension chatInsideViewController {
    
    func navigationBarSetup() {
        
        let navBar: UINavigationBar = UINavigationBar(frame: CGRect(x: 0, y: 0, width: UIDevice.current.width, height: 64))
//        navBar.barTintColor = UIColor.black
        
        if #available(iOS 11.0, *) {
            
            navBar.frame.size.height = 84
        }
        
        navBar.isTranslucent = false
        navBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
        self.view.addSubview(navBar);
        
        let back = UIButton(frame: CGRect(x: 5, y: 25, width: 33.5, height: 33.5))
        back.addTarget(self, action: #selector(backPressed), for: .touchUpInside)
        back.setImage(#imageLiteral(resourceName: "back"), for: .normal)
//        back.backgroundColor = UIColor.red
        navBar.addSubview(back)
        
        let call = UIButton(frame: CGRect(x: CGFloat(UIDevice.current.width - 50), y: 25, width: 33.5, height: 33.5))
        call.addTarget(self, action: #selector(self.callPressed), for: .touchUpInside)
        call.setImage(#imageLiteral(resourceName: "telephone"), for: .normal)
        call.backgroundColor = UIColorFromHex(rgbValue: 0xDC143C)
        call.layer.cornerRadius = 33.5/2
        call.clipsToBounds = true
        navBar.addSubview(call)

        let name = UILabel(frame: CGRect(x: 59, y: 28.5, width: 213, height: 25))
        name.text = hisName.uppercased()
        name.font = UIFont(name: "OpenSans-Regular", size: 16)
        name.textColor = UIColor.black
        navBar.addSubview(name)
        
        self.addSpacingToTitle(navBar, title: "JOHN DOE")
    }
    
    func callPressed() {
        
        print("voip")
        self.inputToolbar.contentView.textView.resignFirstResponder()
//        self.view.resignFirstResponder()
        
//        self.inputToolbar.resignFirstResponder()
        
        if hisImage != nil {
            
            SinchCall.sharedInstance.callUser(friendId, name: hisName, profile: hisImage!)
        }
        else {
            
            
            SinchCall.sharedInstance.callUser(friendId, name: hisName, profile: #imageLiteral(resourceName: "images"))
        }
        
        
    }
    
    func initialSetup() {
        
        self.senderId = "\(self.getUserId())"
        self.senderDisplayName = self.getUsername()
        self.navigationController?.navigationBar.isHidden = true
        
        collectionView!.collectionViewLayout.incomingAvatarViewSize = CGSize(width: 24, height: 24)
        collectionView!.collectionViewLayout.outgoingAvatarViewSize = CGSize(width: 24, height: 24)
        collectionView.collectionViewLayout.messageBubbleFont = UIFont(name: "OpenSans-Regular", size: 14.0)
        self.collectionView.backgroundColor = UIColor.white
        
        //        self.inputToolbar.preferredDefaultHeight = 72.0
        self.inputToolbar.contentView.textView.placeHolder = "Your message here..."
        self.inputToolbar.contentView.textView.textColor = UIColor.black
        self.inputToolbar.contentView.textView.font = UIFont(name: "OpenSans-Regular", size: 14.0)
        
        self.inputToolbar.contentView.rightBarButtonItem.setTitleColor(self.UIColorFromHex(rgbValue: 0xdc143c), for: .highlighted)
        
        self.inputToolbar.contentView.rightBarButtonItem.setTitleColor(self.UIColorFromHex(rgbValue: 0xdc143c), for: UIControlState.normal)
        
        self.inputToolbar.contentView.backgroundColor = UIColor.white
        
        self.inputToolbar.contentView.textView.layer.borderWidth = 0.0
        
        self.inputToolbar.contentView.leftBarButtonItem.setImage(#imageLiteral(resourceName: "plus"), for: .normal)
        self.inputToolbar.contentView.leftBarButtonItem.isEnabled = false
        
        self.inputToolbar.contentView.rightBarButtonItem.setImage(#imageLiteral(resourceName: "chatSend"), for: .normal)
        
        self.inputToolbar.contentView.rightBarButtonItem.setTitle("", for: .normal)
        
        self.topContentAdditionalInset = 61.5

        let imgBackground:UIImageView = UIImageView(frame: self.view.bounds)
        imgBackground.image = #imageLiteral(resourceName: "chat-background.jpg")
        imgBackground.contentMode = UIViewContentMode.scaleAspectFill
        imgBackground.clipsToBounds = true
        imgBackground.alpha = 0.15
        self.collectionView?.backgroundView = imgBackground
    }
    
    fileprivate func setupBubbles() {
        let factory = JSQMessagesBubbleImageFactory()
        outgoingBubbleImageView = factory?.outgoingMessagesBubbleImage(
            with: self.UIColorFromHex(rgbValue: 0xdddddd))
        incomingBubbleImageView = factory?.incomingMessagesBubbleImage(
            with: self.UIColorFromHex(rgbValue: 0xdc143c))
    }
}

///////////////////////
// CHAT SETUP
///////////////////////

extension chatInsideViewController {
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!,
                                 messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        return messages[indexPath.item]
    }
    
    override func collectionView(_ collectionView: UICollectionView,
                                 numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!,
                                 messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let message = messages[indexPath.item]
        if message.senderId == senderId {
            
            return outgoingBubbleImageView
            
        } else {
            
            return incomingBubbleImageView
            
        }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!,
                                 avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        
        let message = messages[indexPath.row]
        
        if message.senderId == self.senderId {
            
            let avatar = JSQMessagesAvatarImageFactory.avatarImage(with: self.getUserImage(), diameter: 35)
            return avatar
        }
        else {
            
            if hisImage != nil {
                
                let avatar = JSQMessagesAvatarImageFactory.avatarImage(with: hisImage!, diameter: 35)
                return avatar
            }
            else {
                
                let avatar = JSQMessagesAvatarImageFactory.avatarImage(with: #imageLiteral(resourceName: "images"), diameter: 35)
                return avatar
            }
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView,
                                 cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let message = messages[indexPath.row]
        
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath)
            as! JSQMessagesCollectionViewCell
        
        if message.senderId == self.senderId {
            cell.textView!.textColor = UIColor.black
        }
        else {
            cell.textView!.textColor = UIColor.white
        }
        
        return cell
    }
    
    func newMessage(_ notification: Notification) {
        
//        print("inside chat")
//        print(notification)
//
//        if let json = notification.object as? JSON {
//
//            print(json)
//
//            if json["uc_id"].stringValue == chatroomId {
//
//                if json["user_id"].stringValue != self.senderId {
//
//                    messages.append(JSQMessage(senderId: json["user_id"].stringValue, displayName: json["full_name"].stringValue, text: json["message"].stringValue))
//
//                    self.finishReceivingMessage()
//                }
//            }
//        }
        
        print("inside chat")
        
        var msg = String()
        var userId = String()
        
        if let data = notification.object as? JSON {
            
            print(data)
            let dataString = data.stringValue
            let dataArray = dataString.components(separatedBy: ",")
            
            if dataArray.indices.contains(0) && dataArray.indices.contains(3) {
                
                if dataArray.indices.contains(0) {
                    
                    let msgArray = dataArray[0].components(separatedBy: ":")
                    
                    if msgArray.indices.contains(1) {
                        
                        msg = dataArray[0].components(separatedBy: ":")[1]
                        msg.remove(at: msg.startIndex)
                        msg.remove(at: msg.index(before: msg.endIndex))
                    }
                }
                
                if dataArray.indices.contains(3) {
                    
                    let msgArray = dataArray[0].components(separatedBy: ":")
                    
                    if msgArray.indices.contains(1) {
                        
                        userId = dataArray[3].components(separatedBy: ":")[1]
                    }
                }
                
                messages.append(JSQMessage(senderId: userId, displayName: "", text: msg))
                self.finishReceivingMessage()
            }
        }
    }
    
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        
        let jsqmsg = JSQMessage(senderId: self.senderId, senderDisplayName: self.senderDisplayName, date: date as Date!, text: text)
        messages.append(jsqmsg!)
        self.sendMessage(uc_id: chatroomId, user_id: "\(self.getUserId())", user_name: self.getUsername(), first_name: self.getUsername(), last_name: self.getUsername(), message: text, date_time: "12/12/2012", profile_pic_url: "what")
        self.finishSendingMessage(animated: true)
        self.hitSendMessage(text)
    }
    
    func hitSendMessage(_ message: String) {
        
        self.postServer2(param: ["uc_id":chatroomId,"user_id":self.senderId,"message":message, "friend_id":friendId, "profile_pic": self.getImageUrl(), "full_name": self.getUsername()], url: K.MEMBO_ENDPOINT + "sendMessage") { [unowned self] (json) in
 
            if json["error"].stringValue == "false" {
                
                
            }
            else {
                
                self.Toast(text: "Something went wrong. Try again later.")
            }
        }
    }
}

/////////////////////
// Socket setup
/////////////////////

extension chatInsideViewController {
    
    func establishConnection(id: String, username: String) {
        
        socket = SocketIOClient(socketURL: URL(string: "http://172.104.54.149:8025")!, config: [.log(false), .compress])
        
        socket?.connect()
        
        socket?.on(clientEvent: .connect) { (data, ack) in
            
            print("socket connected")
            
            print(id)
            print(username)
            
            self.socket?.emit("createRoom", id)
            self.socket?.emit("adduser", username, id)
        }
        
        checkIfOtherUserHasStartedTyping()
        checkIfOtherUserHasStoppedTyping()
        checkIfChatIsUpdated()
        checkIfRoomIsUpdated()
        checkIfRoomIsCreated()
}

    func closeConnection() {
        socket?.disconnect()
        
        socket?.on(clientEvent: .disconnect) { (data, ack) in
            
            print("scoket disconnected")
        }
    }
    
    private func checkIfOtherUserHasStartedTyping() {
        
        socket?.on("typing") { (data, ack) in
            
            print("other user is typing")
        }
    }

    private func checkIfOtherUserHasStoppedTyping() {
        
        socket?.on("stop typing") { (data, ack) in
            
            print("other user has stopped typing")
        }
    }
    
    private func checkIfChatIsUpdated() {
        
        socket?.on("updatechat") { (data, ack) in
            
            print("chat is updated")
            
            if let username = data[0] as? String {
                
                print(username)
            }
            
            if data.indices.contains(1) {
                
                let jsonObject = data[1]
                
                let json = JSON(jsonObject)
                
                NotificationCenter.default.post(name: .newMessage, object: json)
            }
        }
    }

    private func checkIfRoomIsUpdated() {
        
        socket?.on("updaterooms") { (data, ack) in
            
            print("room is updated")
        }
    }
    
    private func checkIfRoomIsCreated() {
        
        socket?.on("roomcreated") { (data, ack) in
            
            print("room is created")
        }
    }
    
    func sendMessage(uc_id: String, user_id: String, user_name: String, first_name: String, last_name: String, message: String, date_time: String, profile_pic_url: String) {
        
        let jsonObject: [String: Any] = [
            "uc_id": uc_id,
            "user_id": user_id,
            "user_name": user_name,
            "first_name": first_name,
            "last_name": last_name,
            "full_name": first_name + " " + last_name,
            "message": message,
            "date_time": date_time,
            "profile_pic": profile_pic_url
        ]
        
        socket?.emit("sendchat", jsonObject)
    }

    func sendStartTypingMessage(nickname: String) {
        
        //emit start typing or something
    }
    
    
    func sendStopTypingMessage(nickname: String) {
        
        //emit stop typing or something
    }
}







