//
//  adminViewController.swift
//  Membo
//
//  Created by Ameya Vichare on 10/10/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit
import PieCharts

class adminViewController: UIViewController, PieChartDelegate {

    @IBOutlet weak var chartView: PieChart!
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var hamButton: UIBarButtonItem!
    @IBOutlet weak var fieldCollection: UICollectionView!
    
    var fields = ["Planners".uppercased(), "Guests".uppercased(), "Timeline posts".uppercased(), "People tracker".uppercased(), "gallery".uppercased(), "request cab".uppercased()]
    var icons = [#imageLiteral(resourceName: "support"),#imageLiteral(resourceName: "team"),#imageLiteral(resourceName: "post-it"),#imageLiteral(resourceName: "tracker"),#imageLiteral(resourceName: "gallery"),#imageLiteral(resourceName: "cab")]
    var accepted = Int()
    var declined = Int()
    var pending = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addSpacingToTitle(navBar, title: "DASHBOARD")
        self.setupHam(navBar, button: hamButton)
        getChartData()
    }
    
    func getChartData() {
        
        self.getServer(url: K.MEMBO_ENDPOINT + "getRsvpChartCount/\(self.getWeddingId())") { [unowned self] (json) in
            
            if json["error"].stringValue == "false" {
                
                self.accepted = Int(json["accepted"].stringValue)!
                self.pending = Int(json["pending"].stringValue)!
                self.declined = Int(json["declined"].stringValue)!
                self.chartView.outerRadius = self.view.frame.size.width * 95/414
                self.chartView.layers = [self.createCustomViewsLayer(), self.createTextLayer()]
                self.chartView.delegate = self
                self.chartView.models = self.createModels() // order is important - models have to be set at the end
            }
            else {
                
                self.Toast(text: "Something went wrong. Try again later.")
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
    
        self.navigationController?.navigationBar.isHidden = true
    }
    
    // MARK: - PieChartDelegate
    
    func onSelected(slice: PieSlice, selected: Bool) {
        print("Selected: \(selected), slice: \(slice)")
    }
    
    // MARK: - Models
    
    fileprivate func createModels() -> [PieSliceModel] {
        return [
            PieSliceModel(value: Double(accepted), color: UIColorFromHex(rgbValue: 0xDD0E3A)),
            PieSliceModel(value: Double(pending), color: UIColorFromHex(rgbValue: 0xA30728)),
            PieSliceModel(value: Double(declined), color: UIColor.black),
        ]
    }
    
    // MARK: - Layers
    
    fileprivate func createCustomViewsLayer() -> PieCustomViewsLayer {
        let viewLayer = PieCustomViewsLayer()
        
        let settings = PieCustomViewsLayerSettings()
        settings.viewRadius = 135
        settings.hideOnOverflow = false
        viewLayer.settings = settings
        
        viewLayer.viewGenerator = createViewGenerator()
        
        return viewLayer
    }
    
    fileprivate func createTextLayer() -> PiePlainTextLayer {
        let textLayerSettings = PiePlainTextLayerSettings()
        textLayerSettings.viewRadius = 60
        textLayerSettings.hideOnOverflow = true
        textLayerSettings.label.textColor = UIColor.white
        textLayerSettings.label.font = UIFont.systemFont(ofSize: 14)
        
//        let formatter = NumberFormatter()
//        formatter.maximumFractionDigits = 1
        textLayerSettings.label.textGenerator = {slice in
            
//            switch slice.data.id {
//            case 0:
            return "\(Int(slice.data.model.value))"
//            case 1:
//                return "\(slice.data.model.value) Pending"
//            case 2:
//                return "\(slice.data.model.value) Guests have declined rsvp"
//            default:
//                return ""
//            }
            
        }
        
        let textLayer = PiePlainTextLayer()
        textLayer.settings = textLayerSettings
        return textLayer
    }
    
    fileprivate func createViewGenerator() -> (PieSlice, CGPoint) -> UIView {
        return {slice, center in
            
            let container = UIView()
            container.frame.size = CGSize(width: 100, height: 40)
//            container.backgroundColor = UIColor.blue
            
            container.center = center

            let specialTextLabel = UILabel()
            specialTextLabel.textAlignment = .center
            specialTextLabel.textColor = UIColor.black
            specialTextLabel.font = UIFont.boldSystemFont(ofSize: 10)
            if slice.data.id == 0 {
                specialTextLabel.text = "Guests accepted RSVP"
                
            } else if slice.data.id == 1 {
                specialTextLabel.text = "Guests pending RSVP"
            }
            else {
                specialTextLabel.text = "Guests declined RSVP"
            }
            specialTextLabel.sizeToFit()
            specialTextLabel.numberOfLines = 2
            specialTextLabel.frame = CGRect(x: 0, y: 0, width: 100, height: 40)
            container.addSubview(specialTextLabel)
            container.frame.size = CGSize(width: 100, height: 40)

            return container
        }
    }
}

extension adminViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "field", for: indexPath) as! fieldCollectionViewCell
        cell.icon.image = icons[indexPath.row]
        cell.lab.text = fields[indexPath.row]
        cell.backgroundColor = UIColor.white
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: self.view.frame.size.width/2 - 0.5, height: 133)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch indexPath.row {
        
        case 0:
            
            let tab = UIStoryboard(name: "HomeTab", bundle: nil)
            if let vc = tab.instantiateViewController(withIdentifier: "chatMainVC") as? chatMainViewController {
                
                vc.isPlanner = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        case 1:
            let tab = UIStoryboard(name: "GuestTab", bundle: nil)
            if let vc = tab.instantiateViewController(withIdentifier: "guestVC") as? guestViewController {
                
                vc.isAdm = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
        case 2:
            let tab = UIStoryboard(name: "HomeTab", bundle: nil)
            if let vc = tab.instantiateViewController(withIdentifier: "homeVC") as? homeFeedViewController {
                
                vc.isAdm = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        case 3:
            let tab = UIStoryboard(name: "TabBar", bundle: nil)
            if let vc = tab.instantiateViewController(withIdentifier: "mapVC") as? mapViewController {
                
                vc.isAdm = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
        
        case 4:
            let tab = UIStoryboard(name: "TabBar", bundle: nil)
            if let vc = tab.instantiateViewController(withIdentifier: "albumVC") as? albumViewController {
                
                vc.isAdm = true
                self.navigationController?.pushViewController(vc, animated: true)
            }

        default:
            break
        }
    }
}
