//
//  addPostViewController.swift
//  Membo
//
//  Created by Ameya Vichare on 08/10/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit

protocol backHome: class {
    
    func reloadIt(pic: UIImage, text: String)
}

class addPostViewController: UIViewController {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var postTable: UITableView!
    
    var selectedImage: UIImage?
    weak var delegate: backHome?
    var postTitle = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.hideKeyboardWhenTappedAround()
        self.addSpacingToTitle(navBar, title: "ADD A MOMENT")
        setupTable()
    }
    
    func setupTable() {
        
        postTable.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 150))
        postTable.estimatedRowHeight = 60
        postTable.rowHeight = UITableViewAutomaticDimension
    }
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func postPressed(_ sender: Any) {
        
        if selectedImage != nil {
            
            var param = [String: Any]()

            if let base64String = UIImageJPEGRepresentation(selectedImage!, 0.7)?.base64EncodedString() {
                
                param = ["title": postTitle, "weddingId": self.getWeddingId(), "isAdmin": self.isAdmin(), "userId": self.getUserId(), "base64String": base64String]
            }
            
            self.postServer(param: param, url: K.MEMBO_ENDPOINT + "postFeed", completion: { (json) in
                
                if json["error"].stringValue == "false" {
                    
                    self.Toast(text: "Posted.")
                    
                    DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
                        
                        self.delegate?.reloadIt(pic: self.selectedImage!, text: self.postTitle)
                        self.navigationController?.popViewController(animated: true)
                    })
                }
                else {
                    
                    self.Toast(text: "Something went wrong. Try again later")
                }
            })
        }
        else {
            
            self.Toast(text: "Select an image.")
        }
    }
}

extension addPostViewController: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        
        let startHeight = textView.frame.size.height
        let calcHeight = textView.sizeThatFits(textView.frame.size).height
        
        if startHeight != calcHeight {
            
            UIView.setAnimationsEnabled(false)
            self.postTable.beginUpdates()
            self.postTable.endUpdates()
            UIView.setAnimationsEnabled(true)
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        postTitle = textView.text
    }
}

extension addPostViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "title", for: indexPath) as! postTitleTableViewCell
            
            cell.selectionStyle = .none
            return cell
        }
        else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "image", for: indexPath) as! postImageTableViewCell
            
            if selectedImage != nil {
                
                cell.imageV.image = selectedImage
            }
            
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 1 {
            
            //image selection
            
//            let actionSheet = UIAlertController(title: "Choose photo from", message: nil , preferredStyle: .actionSheet)
//
//            actionSheet.addAction(UIAlertAction(title: "Camera", style: .default , handler: { [unowned self]
//                (action: UIAlertAction) -> Void in

                let image = UIImagePickerController()
                image.delegate = self
                image.sourceType = UIImagePickerControllerSourceType.camera
//                image.mediaTypes = ["public.image"]
//                image.allowsEditing = true
                self.present(image, animated: true, completion: nil)
                
//            }))
            
//            actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default , handler: { [unowned self]
//                (action: UIAlertAction) -> Void in
//
//                let image = UIImagePickerController()
//                image.delegate = self
//                image.sourceType = UIImagePickerControllerSourceType.photoLibrary
//                image.mediaTypes = ["public.image"]
////                image.allowsEditing = true
//                self.present(image, animated: true, completion: nil)
//            }))
//
//            actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel , handler: { [unowned self]
//                (action: UIAlertAction) -> Void in
//                actionSheet.dismiss(animated: true, completion: nil)
//            }))
//
//            present(actionSheet, animated: true, completion:nil)

        }
    }
}

extension addPostViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            selectedImage = image
            DispatchQueue.main.async {
                
                self.postTable.reloadData()
            }
        }
        self.dismiss(animated: true, completion: nil)
    }
}
