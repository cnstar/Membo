//
//  SocketIOManager.swift
//  Membo
//
//  Created by Ameya Vichare on 13/10/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit
import SocketIO
import SwiftyJSON

extension Notification.Name {
    static let online = Notification.Name("online")
}

class SocketIOManager: NSObject {
    static let sharedInstance = SocketIOManager()
    
    var socket : SocketIOClient
    
    override init() {
        
        self.socket = SocketIOClient(socketURL: URL(string: "http://172.104.54.149:8025")!, config: [.log(false), .compress])
    }
    
    func establishConnection(id: String, username: String) {
        
        socket.connect()
        
        socket.on(clientEvent: .connect) { (data, ack) in
            
            print("socket connected")
            
            print(id)
            print(username)
            
            self.socket.emit("createRoom", id)
            self.socket.emit("adduser", username, id)
        }
        
        checkIfOtherUserHasStartedTyping()
        checkIfOtherUserHasStoppedTyping()
        checkIfChatIsUpdated()
        checkIfRoomIsUpdated()
        checkIfRoomIsCreated()
    }
    
    func establishConnectionForRealTimeStatus(id: String) {
        
        socket.connect()
        
        socket.on(clientEvent: .connect) { (data, ack) in
            
            print("socket connected for real time chat")
            
            self.socket.emit("joinUser", id)
        }
        
        checkIfUserIsOnline()
    }
    
    private func checkIfUserIsOnline() {
        
        socket.on("receive") { (data, ack) in
            
            print("-------------------------------------")
            print(data)
            
            if var userIdArray = data[2] as? String {
                
                print("-------------------------------------")
                
                if userIdArray.isNotEmpty {
                    
                    userIdArray.removeFirst()
                    userIdArray.removeLast()
                }

                var userIds = userIdArray.components(separatedBy: ",")
                
                if userIds.count > 0 {
                    
                    for i in 0..<userIds.count {
                        
                        if userIds[i].isNotEmpty {
                            
                            userIds[i].removeFirst()
                            userIds[i].removeLast()
                        }
                    }
                    print("testData: \(userIds)")
                    UserDefaults.standard.set(userIds, forKey: "onlineUsers")
                    
                    NotificationCenter.default.post(name: .online, object: userIds)
                }
            }
        }
    }
    
    func closeConnection() {
        socket.disconnect()
        
        socket.on(clientEvent: .disconnect) { (data, ack) in
            
            print("scoket disconnected")
        }
    }
    
    /////////////////////////////////
    // DATA FROM OTHER USER
    /////////////////////////////////
    
    private func checkIfOtherUserHasStartedTyping() {
        
        socket.on("typing") { (data, ack) in
            
            print("other user is typing")
        }
    }
    
    private func checkIfOtherUserHasStoppedTyping() {
        
        socket.on("stop typing") { (data, ack) in
            
            print("other user has stopped typing")
        }
    }
    
    private func checkIfChatIsUpdated() {
        
        socket.on("updatechat") { (data, ack) in
            
            print("chat is updated")
            
            if let username = data[0] as? String {
                
                print(username)
            }
            
            if data.indices.contains(1) {
                
                let jsonObject = data[1]
                
                let json = JSON(jsonObject)
                
                NotificationCenter.default.post(name: .newMessage, object: json)
            }
        }
    }
    
    private func checkIfRoomIsUpdated() {
        
        socket.on("updaterooms") { (data, ack) in
            
            print("room is updated")
        }
    }
    
    private func checkIfRoomIsCreated() {
        
        socket.on("roomcreated") { (data, ack) in
            
            print("room is created")
        }
    }
    
    
    //////////////////////////////////
    // emit data
    //////////////////////////////////
    
    func sendMessage(uc_id: String, user_id: String, user_name: String, first_name: String, last_name: String, message: String, date_time: String, profile_pic_url: String) {
        
        let jsonObject: [String: Any] = [
            "uc_id": uc_id,
            "user_id": user_id,
            "user_name": user_name,
            "first_name": first_name,
            "last_name": last_name,
            "full_name": first_name + " " + last_name,
            "message": message,
            "date_time": date_time,
            "profile_pic": profile_pic_url
        ]
        
        socket.emit("sendchat", jsonObject)
    }
    
    func sendStartTypingMessage(nickname: String) {
        
        //emit start typing or something
    }
    
    
    func sendStopTypingMessage(nickname: String) {
        
        //emit stop typing or something
    }
    
    
    //    func connectToServerWithNickname(nickname: String, completionHandler: @escaping (_ userList: [[String: AnyObject]]?) -> Void) {
    //        socket.emit("connectUser", nickname)
    //
    //        socket.on("userList") { ( dataArray, ack) -> Void in
    //            completionHandler(dataArray[0] as? [[String: AnyObject]])
    //        }
    //
    //        listenForOtherMessages()
    //    }
    
    
    //    private func listenForOtherMessages() {
    //        socket.on("userConnectUpdate") { (dataArray, socketAck) -> Void in
    //            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userWasConnectedNotification"), object: dataArray[0] as! [String: AnyObject])
    //        }
    //
    //        socket.on("userExitUpdate") { (dataArray, socketAck) -> Void in
    //            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userWasDisconnectedNotification"), object: dataArray[0] as! String)
    //        }
    //
    //        socket.on("userTypingUpdate") { (dataArray, socketAck) -> Void in
    //            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userTypingNotification"), object: dataArray[0] as? [String: AnyObject])
    //        }
    //    }
    
    //    func exitChatWithNickname(nickname: String, completionHandler: () -> Void) {
    //        socket.emit("exitUser", nickname)
    //        completionHandler()
    //    }
}

