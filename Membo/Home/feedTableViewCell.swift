//
//  feedTableViewCell.swift
//  Membo
//
//  Created by Ameya Vichare on 24/09/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit

protocol feedAction: class {
    
    func likeIt(_ row: Int)
    func expressIt(_ row: Int)
    func tapIt(_ row: Int)
    func whoLiked(_ row: Int)
}

class feedTableViewCell: UITableViewCell {
    
    weak var delegate: feedAction?
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var likeCount: UILabel!
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var feedPic: UIImageView!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var expressButton: UIButton!
    @IBOutlet weak var picTapButton: UIButton!
    @IBOutlet weak var whoLikedButton: UIButton!
    
    override func prepareForReuse() {
        
        feedPic.sd_cancelCurrentImageLoad()
        profilePic.sd_cancelCurrentImageLoad()
    }
    
    @IBAction func whoLikedPressed(_ sender: UIButton) {
        self.delegate?.whoLiked(sender.tag)
    }
    
    @IBAction func likePressed(_ sender: UIButton) {
        self.delegate?.likeIt(sender.tag)
    }
    
    @IBAction func expressPressed(_ sender: UIButton) {
        self.delegate?.expressIt(sender.tag)
    }
    
    @IBAction func picTapped(_ sender: UIButton) {
        self.delegate?.tapIt(sender.tag)
    }
}
