//
//  Comment.swift
//  Alamofire
//
//  Created by Ameya Vichare on 03/10/17.
//

import Foundation

class Comment {
    
    var userId: String
    var commentId: String
    var message: String
    var status: String
    var profilePic: UIImage
    var dateAdded: String
    var guestName: String
    
    init(userId: String, commentId: String, message: String, status: String, profilePic: UIImage, dateAdded: String, guestName: String) {
        
        self.userId = userId
        self.commentId = commentId
        self.message = message
        self.status = status
        self.profilePic = profilePic
        self.dateAdded = dateAdded
        self.status = status
        self.guestName = guestName
    }
}
