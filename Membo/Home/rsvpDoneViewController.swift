//
//  rsvpDoneViewController.swift
//  Membo
//
//  Created by Ameya Vichare on 24/09/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit

class rsvpDoneViewController: UIViewController {

    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var navBar: UINavigationBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.addSpacingToTitle(navBar, title: "RSVP")
        setupButtons(okButton)
    }
    
    func setupButtons(_ button: UIButton) {
        
        button.layer.cornerRadius = 20.5
        button.clipsToBounds = true
    }
    
    @IBAction func okPressed(_ sender: Any) {
        
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
    self.navigationController!.popToViewController(viewControllers[viewControllers.count - 4], animated: true)
    }
}
