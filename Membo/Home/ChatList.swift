//
//  ChatList.swift
//  Membo
//
//  Created by Ameya Vichare on 12/10/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import Foundation

import Foundation

class UserChatList {
    
    var userName: String
    var userId: String
    var image: String
    var firstName: String
    var lastName: String
    var chatroomId: String
    var lastMessage: String
    var dateTime: String
    var timesElapsed: String
    
    init(userName: String, userId: String,image:String, firstName: String, lastName: String, chatroomId: String, lastMessage: String, dateTime:String, timesElapsed:String) {
        self.userName = userName
        self.userId = userId
        self.image = image
        self.firstName = firstName
        self.lastName = lastName
        self.chatroomId = chatroomId
        self.lastMessage = lastMessage
        self.dateTime = dateTime
        self.timesElapsed = timesElapsed
    }
}
