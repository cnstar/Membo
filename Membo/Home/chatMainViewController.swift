//
//  chatMainViewController.swift
//  Membo
//
//  Created by Ameya Vichare on 25/09/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit
import SDWebImage

class chatMainViewController: UIViewController {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var chatTable: UITableView!
    @IBOutlet weak var plusbutton: UIBarButtonItem!
    
    var list = [UserChatList]()
    var isPlanner = Bool()
    var onlineUsers = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTable()
        
        if !isPlanner {
            
            self.addSpacingToTitle(navBar, title: "CHATS")
            getRecentChats()
        }
        else {
            
            self.addSpacingToTitle(navBar, title: "PLANNERS")
            getPlanners()
            plusbutton.image = nil
            plusbutton.isEnabled = false
        }
        
    }
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        
        return refreshControl
    }()
    
    func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        getRecentChats()
    }
    
    @IBAction func newPressed(_ sender: Any) {
        
        let tab = UIStoryboard(name: "GuestTab", bundle: nil)
        if let vc = tab.instantiateViewController(withIdentifier: "guestVC") as? guestViewController {
            
            vc.isNewChat = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func getRecentChats() {
        
        list.removeAll()
        DispatchQueue.main.async {
            self.chatTable.reloadData()
        }
        
        self.getServer(url: K.MEMBO_ENDPOINT + "getResentChat/\(self.getUserId())") { (json) in
            
            for item in json["chat_data"].arrayValue {
                
                self.list.append(UserChatList(userName: item["userName"].stringValue, userId: item["userId"].stringValue, image: item["profilePic"].stringValue, firstName: item["firstName"].stringValue, lastName: "0", chatroomId: item["ucId"].stringValue, lastMessage: item["message"].stringValue, dateTime: item["dateTime"].stringValue, timesElapsed: item["timesElapsed"].stringValue))
                
                DispatchQueue.main.async {
                    
                    self.chatTable.reloadData()
                }
                
                self.checkStoredOnlineUsers()
            }
        }
    }
    
    func getPlanners() {
        
        list.removeAll()
        DispatchQueue.main.async {
            self.chatTable.reloadData()
        }
        
        self.getServer(url: K.MEMBO_ENDPOINT + "getAdminList/\(self.getWeddingId())") { (json) in
            
            for item in json["guestList"].arrayValue {
                
                self.list.append(UserChatList(userName: item["guestName"].stringValue, userId: item["userId"].stringValue, image: item["profilePic"].stringValue, firstName: item["mobileNo"].stringValue, lastName: item["guestList"].stringValue, chatroomId: "", lastMessage: item["email"].stringValue, dateTime: item["group_name"].stringValue, timesElapsed: ""))
                
                DispatchQueue.main.async {
                    
                    self.chatTable.reloadData()
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadOnline(_:)), name: .online, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        NotificationCenter.default.removeObserver(self)
    }
    
    func reloadOnline(_ notification: Notification) {
        
        if let userId = notification.object as? [String] {
            
            onlineUsers = userId
            print(userId)
            changeOnlineUI()
        }
    }
    
    func checkStoredOnlineUsers() {
        
        if var users = UserDefaults.standard.value(forKey: "onlineUsers") as? [String] {
            
            for i in 0..<list.count {
                
                for j in 0..<users.count {
                    
                    if users[j] == list[i].userId {
                        
                        list[i].lastName = "1"
                    }
                    else {
                        
                        list[i].lastName = "0"
                    }
                }
            }
            
            DispatchQueue.main.async {
                
                self.chatTable.reloadData()
            }
        }
    }
    
    func changeOnlineUI() {
        
        for i in 0..<list.count {
            
            for j in 0..<onlineUsers.count {
                
                if onlineUsers[j] == list[i].userId {
                    
                    list[i].lastName = "1"
                }
            }
        }
        
        DispatchQueue.main.async {
            
            self.chatTable.reloadData()
        }
    }
    
    func setupTable() {
        
        chatTable.estimatedRowHeight = 40
        chatTable.rowHeight = UITableViewAutomaticDimension
        chatTable.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
}

extension chatMainViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var lis: UserChatList
        lis = list[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "chat", for: indexPath) as! chatMainTableViewCell
        
        cell.pic.layer.cornerRadius = self.view.frame.size.width * 22.5/414
        cell.pic.clipsToBounds = true
        cell.pic.sd_setImage(with: URL(string: lis.image), placeholderImage: #imageLiteral(resourceName: "images"))
        cell.name.text = lis.userName
        cell.location.text = lis.lastMessage
        cell.status.text = lis.timesElapsed
        cell.pic.layer.borderWidth = 2
        
        if lis.lastName == "1" {
            
            cell.pic.layer.borderColor = UIColor.green.cgColor
        }
        else {
            
            cell.pic.layer.borderColor = UIColor.red.cgColor
        }
        
        cell.selectionStyle = .none
        return cell
    }
}

extension chatMainViewController: updateChat {
    
    func updateIt() {
     
        self.getRecentChats()
    }
}

extension chatMainViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if !isPlanner {
            
            var li: UserChatList
            li = list[indexPath.row]
            
            let cell = chatTable.cellForRow(at: indexPath) as! chatMainTableViewCell
            
            if let vc = storyboard?.instantiateViewController(withIdentifier: "chatInsideVC") as? chatInsideViewController {
                
                vc.hisImage = cell.pic.image!
                vc.delegate = self
                vc.friendId = li.userId
                vc.hisName = li.userName
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        else {
            
            guard let cell = tableView.cellForRow(at: indexPath) as? chatMainTableViewCell else { return }
            
            var li: UserChatList
            li = list[indexPath.row]
            let guest = Guest(userId: li.userId, guestName: li.userName, mobileNo: li.firstName, email: li.lastMessage, profilePic: li.image, gender: li.lastName, location: li.dateTime, guestFrom: "", guestRelation: "", countryCode: "", isVip: "", rsvpStatus: "")
            
            let tab = UIStoryboard(name: "ProfileTab", bundle: nil)

            if let vc = tab.instantiateViewController(withIdentifier: "guestProfileVC") as? guestProfileViewController {
                
                vc.isPlanner = true
                vc.guest = guest
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
}
