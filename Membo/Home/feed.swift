//
//  feed.swift
//  Membo
//
//  Created by Ameya Vichare on 03/10/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import Foundation

class Feed {
    
    var feed_id: String
    var title: String
    var user_id: String
    var userName: String
    var profilePic: String
    var image: String
    var gender: String
    var status: String
    var likeCount: String
    var islike: String
    var commentCount: String
    var dateTime: String
    var timeElapsed: String
    
    init(feed_id: String, title: String, user_id: String, userName: String, profilePic: String, image: String, gender: String, status: String, likeCount: String, islike: String, commentCount: String, dateTime: String, timeElapsed: String) {
        
        self.feed_id = feed_id
        self.title = title
        self.user_id = user_id
        self.userName = userName
        self.profilePic = profilePic
        self.image = image
        self.gender = gender
        self.status = status
        self.likeCount = likeCount
        self.islike = islike
        self.commentCount = commentCount
        self.dateTime = dateTime
        self.timeElapsed = timeElapsed
    }
}
