//
//  Ride.swift
//  Membo
//
//  Created by Ameya Vichare on 11/10/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import Foundation

class Ride {
    
    var pdId: String
    var userId: String
    var pickupDropLocation: String
    var pdGeoLocation: String
    var flightNumber: String
    var flightName: String
    var latitude: String
    var longitude: String
    var driverId: String
    var pdsId: String
    var statusText: String
    var date: String
    var time: String
    var dGender: String
    var dName: String
    var cName: String
    var dPic: String
    var dId: String
    var dMobile: String
    var cNumber: String
    var cType: String
    var cColor: String
    
    init(pdId: String, userId: String, pickupDropLocation: String, pdGeoLocation: String, flightNumber: String, flightName: String, latitude: String, longitude: String, driverId: String, pdsId: String, statusText: String, date: String, time: String, dGender: String, dName: String, cName: String, dPic: String, dId: String, dMobile: String, cNumber: String, cType: String, cColor: String) {
        
        self.pdId = pdId
        self.userId = userId
        self.pickupDropLocation = pickupDropLocation
        self.pdGeoLocation = pdGeoLocation
        self.flightNumber = flightNumber
        self.flightName = flightName
        self.latitude = latitude
        self.longitude = longitude
        self.driverId = driverId
        self.pdsId = pdsId
        self.statusText = statusText
        self.date = date
        self.time = time
        self.dGender = dGender
        self.dName = dName
        self.cName = cName
        self.dPic = dPic
        self.dId = dId
        self.dMobile = dMobile
        self.cNumber = cNumber
        self.cType = cType
        self.cColor = cColor
    }
}
