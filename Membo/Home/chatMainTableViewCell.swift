//
//  chatMainTableViewCell.swift
//  Membo
//
//  Created by Ameya Vichare on 25/09/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit

class chatMainTableViewCell: UITableViewCell {

    @IBOutlet weak var pic: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var status: UILabel!
}
