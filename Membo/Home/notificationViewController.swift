//
//  notificationViewController.swift
//  Membo
//
//  Created by Ameya Vichare on 24/09/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit
import UIEmptyState

class notificationViewController: UIViewController, UIEmptyStateDataSource, UIEmptyStateDelegate {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var notifTable: UITableView!
    
    var type = [Int]()
    
    var notif = [NotificationMembo]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addSpacingToTitle(navBar, title: "NOTIFICATIONS")
        setupTable()
        getNotifications()
        print(self.getUserId())
        self.emptyStateDataSource = self
        self.emptyStateDelegate = self
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        self.emptyStateDataSource = nil
        self.emptyStateDelegate = nil
    }
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupTable() {
        
        notifTable.estimatedRowHeight = 40
        notifTable.rowHeight = UITableViewAutomaticDimension
        notifTable.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    var emptyStateImage: UIImage? {
        return #imageLiteral(resourceName: "notifications-button")
    }
    
    var emptyStateTitle: NSAttributedString {
        
        let attrs = [NSForegroundColorAttributeName: UIColor.black,
                     NSFontAttributeName: UIFont(name: "OpenSans-SemiBold", size: 20)]
        return NSAttributedString(string: "No Notifications found.", attributes: attrs)
    }
    
    var emptyStateDetailMessage: NSAttributedString? {
        
        let attrs = [NSForegroundColorAttributeName: UIColorFromHex(rgbValue: 0x333333),
                     NSFontAttributeName: UIFont(name: "OpenSans-Regular", size: 16)]
        
        return NSAttributedString(string: "Looks like you haven't received any Notifications.", attributes: attrs)
    }
    
    func getNotifications() {
        
        self.getServer(url: K.MEMBO_ENDPOINT + "getNotifications/\(self.getUserId())/\(self.isAdmin())") { [unowned self] (json) in
            
            print(json)
            print(self.getUserId())
            print(self.isAdmin())
            
            if json["error"].stringValue == "false" {
                
                for item in json["notification"].arrayValue {
                    
                    self.notif.append(NotificationMembo(notification_id: item["notification_id"].stringValue, user_id: item["title"].stringValue, sender_id: item["sender_id"].stringValue, notification_type: item["notification_type"].stringValue, description: item["description"].stringValue, activity: item["activity"].stringValue, date_time: item["date_time"].stringValue))
                    
                    DispatchQueue.main.async {
                        
                        self.notifTable.reloadData()
                        self.reloadEmptyStateForTableView(self.notifTable)
                    }
                }
            }
            else {
                
                self.Toast(text: json["message"].stringValue)
            }
        }
    }
}

extension notificationViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return notif.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "notification", for: indexPath) as! notificationTableViewCell
        
        cell.title.text = notif[indexPath.row].user_id
        cell.from.text = notif[indexPath.row].description
        cell.pic.image = #imageLiteral(resourceName: "notifications-button")
        cell.time.text = notif[indexPath.row].date_time
        
        cell.selectionStyle = .none
        return cell
    }
}

extension notificationViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch notif[indexPath.row].activity {
        case "0":
            if let vc = storyboard?.instantiateViewController(withIdentifier: "locReq") as? locReqViewController {

                self.navigationController?.pushViewController(vc, animated: true)
            }
        case "2":
            let tab = UIStoryboard(name: "TabBar", bundle: nil)
            if let vc = tab.instantiateViewController(withIdentifier: "mapVC") as? mapViewController {

                vc.isAdm = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
        case "5":
            let tab = UIStoryboard(name: "TabBar", bundle: nil)
            if let vc = tab.instantiateViewController(withIdentifier: "facilityVC") as? facilitiesViewController {
                
                vc.isRSVP = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
        default:
            break
        }
    }
}
