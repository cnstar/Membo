//
//  rsvpDetailViewController.swift
//  Membo
//
//  Created by Ameya Vichare on 24/09/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit
import TextFieldEffects

class rsvpDetailViewController: UIViewController {

    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var adult: HoshiTextField!
    @IBOutlet weak var children: HoshiTextField!
    @IBOutlet weak var upConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.hideKeyboardWhenTappedAround()
        self.addSpacingToTitle(navBar, title: "RSVP")
        children.text = "0"
        adult.text = "1"
        setupButtons(sendButton)
        handleTextfields(adult)
        handleTextfields(children)
    }
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupButtons(_ button: UIButton) {
        
        button.layer.cornerRadius = 20.5
        button.clipsToBounds = true
    }
    
    func handleTextfields(_ textField: HoshiTextField) {
        
        textField.placeholderColor = UIColorFromHex(rgbValue: 0x999999)
        textField.textColor = UIColor.black
        textField.borderInactiveColor = UIColorFromHex(rgbValue: 0x999999)
        textField.borderActiveColor = UIColorFromHex(rgbValue: 0x999999)
        textField.placeholderFontScale = 0.9
    }
    
    @IBAction func sendPressed(_ sender: Any) {
        
        if Int(adult.text!)! > 0 {
            
            self.postServer(param: ["userId": self.getUserId(), "weddingId": self.getWeddingId(), "adultCount": adult.text!, "childCount": children.text!, "rsvpStatus": 1], url: K.MEMBO_ENDPOINT + "submitRsvp", completion: { [unowned self] (json) in
                
                print(json)
                
                if json["error"].stringValue == "false" {
                    
                    UserDefaults.standard.set("1", forKey: "rsvpStatus")
                    
                    if let vc = self.storyboard?.instantiateViewController(withIdentifier: "rsvpDoneVC") as? rsvpDoneViewController {
                        
                        DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
                            
                            self.navigationController?.pushViewController(vc, animated: true)
                        })
                    }
                }
                else {
                    
                    self.Toast(text: "Something wen't wrong. Try again later.")
                }
            })
        }
        else {
            
            self.Toast(text: "Adult count can't be zero.")
        }
        
    }
}

extension rsvpDetailViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        self.upConstraint.constant = -100
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        self.upConstraint.constant = 10
    }
}
