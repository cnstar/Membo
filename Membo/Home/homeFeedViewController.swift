//
//  homeFeedViewController.swift
//  Membo
//
//  Created by Ameya Vichare on 24/09/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit
import SDWebImage
import StoreKit

extension Notification.Name {
    static let hamOpen = Notification.Name("hamOpen")
    static let hamClose = Notification.Name("hamClose")
}

class homeFeedViewController: UIViewController {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var hamButton: UIBarButtonItem!
    @IBOutlet weak var feedTable: UITableView!
    @IBOutlet weak var float: UIButton!
    
    var feed = [Feed]()
    var currentRow = Int()
    var isAdm = Bool()
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        
        return refreshControl
    }()
    
    func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        getFeed()
    }
    
    deinit {
        
        print("homeFeedViewController deinit")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addSpacingToTitle(navBar, title: "HOME")
        self.feedTable.addSubview(refreshControl)

//        let revealController: SWRevealViewController? = revealViewController()
        revealViewController().delegate = self

        if isAdm {
            
            hamButton.image = #imageLiteral(resourceName: "back")
        }
        else {
            
            self.setupHam(navBar, button: hamButton)
        }
        
        setupTable()
        getFeed()
    }
    
    @IBAction func hamPress(_ sender: Any) {
        
        if isAdm {
            
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    
    func startRotating(duration: Double = 0.3, view: UIView) {
        let kAnimationKey = "rotation"
        
        if view.layer.animation(forKey: kAnimationKey) == nil {
            let animate = CABasicAnimation(keyPath: "transform.rotation")
            animate.duration = duration
            animate.repeatCount = 2
            animate.fromValue = Float(Double.pi * 2.0)
            animate.toValue = 0.0
            animate.delegate = self
            view.layer.add(animate, forKey: kAnimationKey)
        }
    }
    func stopRotating(view: UIView) {
        
    }
    
    func showAlert() {
        
        if let number = UserDefaults.standard.value(forKey: "appOpened") as? Int {
            
            if number > 10 {
                
                SKStoreReviewController.requestReview()
                UserDefaults.standard.set(0, forKey: "appOpened")
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        showAlert()
        self.view.layoutIfNeeded()
        
        if let shouldSplash = UserDefaults.standard.value(forKey: "shouldSplash") as? Bool {
            
            if shouldSplash {
                
                self.handleSplash()
                UserDefaults.standard.set(false, forKey: "shouldSplash")
            }
        }
        else {
            
            self.handleSplash()
            UserDefaults.standard.set(false, forKey: "shouldSplash")
        }
        
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.navigationBar.barStyle = .default
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    @IBAction func floatPressed(_ sender: UIButton) {
        
        if let vc = storyboard?.instantiateViewController(withIdentifier: "addPost") as? addPostViewController {
            
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func likeAnimation(likeImageView: UIImageView) {
        
        startRotating(view: likeImageView)
    }

    func likeEnlarge(cell: feedTableViewCell) {
        
        let enlargeImageView = UIImageView(frame: CGRect(x: self.view.frame.size.width/2 - 20, y: cell.frame.size.height/2 + 20, width: 40, height: 40))
        enlargeImageView.image = #imageLiteral(resourceName: "likeInside")
        enlargeImageView.alpha = 0
        cell.addSubview(enlargeImageView)
        
        enlargeImageView.transform = CGAffineTransform(scaleX: 0.3, y: 0.3)
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
            
            enlargeImageView.alpha = 1
            enlargeImageView.transform = CGAffineTransform(scaleX: 5, y: 5)
            
        }) { (result) in
            
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
                
                enlargeImageView.transform = CGAffineTransform(scaleX: 0.3, y: 0.3)
                
            }) { (result) in
                
                enlargeImageView.alpha = 0
                enlargeImageView.removeFromSuperview()
            }
        }
    }
    
    func getFeed() {
        
        feed.removeAll()
        DispatchQueue.main.async {
            
            self.feedTable.reloadData()
        }
        
        self.getServer(url: K.MEMBO_ENDPOINT + "getFeeds/\(self.getUserId())/\(self.getWeddingId())") { [unowned self] (json) in
            
            print(json)
            
            self.refreshControl.endRefreshing()
            
            for item in json["feeds"].arrayValue {

                self.feed.append(Feed(feed_id: item["feed_id"].stringValue, title: item["title"].stringValue, user_id: item["user_id"].stringValue, userName: item["userName"].stringValue, profilePic: item["profilePic"].stringValue, image: item["image"].stringValue, gender: item["gender"].stringValue, status: item["status"].stringValue, likeCount: item["likes"].stringValue, islike: item["islike"].stringValue, commentCount: item["comments"].stringValue, dateTime: item["dateTime"].stringValue, timeElapsed: item["timeElapsed"].stringValue))

                DispatchQueue.main.async {
                    
                    self.feedTable.reloadData()
                }
            }
        }
    }
    
    func setupTable() {
        
        feedTable.estimatedRowHeight = 200
        feedTable.rowHeight = UITableViewAutomaticDimension
    }
    
    @IBAction func notifPressed(_ sender: Any) {
        
        if let vc = storyboard?.instantiateViewController(withIdentifier: "notifVC") as? notificationViewController {
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func chatPressed(_ sender: Any) {

        if let vc = storyboard?.instantiateViewController(withIdentifier: "chatMainVC") as? chatMainViewController {
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension homeFeedViewController: CAAnimationDelegate {
    
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        
        var fe: Feed
        fe = feed[currentRow]
        
        let kAnimationKey = "rotation"
        
        if view.layer.animation(forKey: kAnimationKey) != nil {
            view.layer.removeAnimation(forKey: kAnimationKey)
        }
        
        let index = IndexPath(row: currentRow, section: 0)
        let cell = feedTable.cellForRow(at: index) as! feedTableViewCell
        
        if fe.islike == "1" {
            
            fe.islike = "0"
            fe.likeCount = String(Int(fe.likeCount)! - 1)
            cell.likeCount.text = fe.likeCount
            cell.likeButton.setTitle("Like", for: .normal)
            cell.likeButton.setImage(#imageLiteral(resourceName: "likeHome"), for: .normal)
        }
        else {
            
            self.likeEnlarge(cell: cell)
            fe.islike = "1"
            fe.likeCount = String(Int(fe.likeCount)! + 1)
            cell.likeCount.text = fe.likeCount
            cell.likeButton.setTitle("Liked", for: .normal)
            cell.likeButton.setImage(#imageLiteral(resourceName: "likeInside"), for: .normal)
        }
    }
}

extension homeFeedViewController: backHome {
    
    func reloadIt(pic: UIImage, text: String) {
        
        getFeed()
    }
}

extension homeFeedViewController: SWRevealViewControllerDelegate {

    func revealController(_ revealController: SWRevealViewController!, willMoveTo position: FrontViewPosition) {
        
        print("okay")
        
        if position == .left {

            NotificationCenter.default.post(name: .hamClose, object: nil)
            self.view.isUserInteractionEnabled = true
            self.tabBarController?.tabBar.isUserInteractionEnabled = true
        }
        else {

            NotificationCenter.default.post(name: .hamOpen, object: nil)
            self.view.isUserInteractionEnabled = false
            self.tabBarController?.tabBar.isUserInteractionEnabled = false
        }
    }
}


extension homeFeedViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return feed.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var fe: Feed
        fe = feed[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "feed", for: indexPath) as! feedTableViewCell
        cell.delegate = self
        cell.profilePic.sd_setImage(with: URL(string: fe.profilePic), placeholderImage: #imageLiteral(resourceName: "images"))
        cell.title.text = fe.userName
        cell.message.text = fe.title
        cell.time.text = fe.timeElapsed
        cell.feedPic.sd_setImage(with: URL(string: fe.image), placeholderImage: #imageLiteral(resourceName: "images"))
        cell.likeCount.text = fe.likeCount
        makeItRound(cell.profilePic)
        cell.likeButton.tag = indexPath.row
        cell.expressButton.tag = indexPath.row
        cell.picTapButton.tag = indexPath.row
        cell.whoLikedButton.tag = indexPath.row
        
        if fe.islike == "1" {
            
            cell.likeButton.setImage(#imageLiteral(resourceName: "likeInside"), for: .normal)
            cell.likeButton.setTitle("Liked", for: .normal)
        }
        else {
            
            cell.likeButton.setImage(#imageLiteral(resourceName: "likeHome"), for: .normal)
            cell.likeButton.setTitle("Like", for: .normal)
        }
        
        cell.selectionStyle = .none
        return cell
    }
    
    func makeItRound(_ pic: UIImageView) {
        
        pic.layer.cornerRadius = self.view.frame.size.width * 22.5/414
        pic.clipsToBounds = true
    }
}

extension homeFeedViewController: backFromEnlarge {
    
    func backFromEnlargePressed(feedBack: Feed, row: Int) {
        
        var fe: Feed
        fe = feed[row]
        fe = feedBack
        
        DispatchQueue.main.async {
            
            self.feedTable.reloadData()
        }
        
        self.setTabBarVisible(visible: !self.tabBarIsVisible(), animated: true)
        self.tabBarController?.tabBar.frame.origin.y = self.view.frame.size.height - 49
    }
}

extension homeFeedViewController: feedAction {
    
    func whoLiked(_ row: Int) {
        
        var fe: Feed
        fe = feed[row]
        
        let tab = UIStoryboard(name: "GuestTab", bundle: nil)
        
        if let vc = tab.instantiateViewController(withIdentifier: "guestVC") as? guestViewController {
            
            vc.feedId = fe.feed_id
            vc.isNewChat = true
            vc.isWhoLiked = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func likeIt(_ row: Int) {
        
        var fe: Feed
        fe = feed[row]
        
        currentRow = row
        let index = IndexPath(row: row, section: 0)
        let cell = feedTable.cellForRow(at: index) as! feedTableViewCell
        
        likeAnimation(likeImageView: cell.likeButton.imageView!)
        
        self.postServer2(param: ["user_id": self.getUserId(), "feed_id": fe.feed_id], url: K.MEMBO_ENDPOINT + "postUserLikePhoto") { [unowned self] (json) in
            
            if json["error"].stringValue == "false" {
                
                if fe.islike == "1" {
                    
//                    fe.islike = "0"
//                    fe.likeCount = String(Int(fe.likeCount)! - 1)
//                    cell.likeButton.setTitle(fe.likeCount + " Likes", for: .normal)
//                    cell.likeButton.setImage(#imageLiteral(resourceName: "likeHome"), for: .normal)
                }
                else {
                    
//                    self.likeEnlarge(cell: cell)
//                    fe.islike = "1"
//                    fe.likeCount = String(Int(fe.likeCount)! + 1)
//                    cell.likeButton.setTitle(fe.likeCount + " Likes", for: .normal)
//                    cell.likeButton.setImage(#imageLiteral(resourceName: "likeInside"), for: .normal)
                }
            }
            else if json["error"].stringValue == "true" {
                
                self.Toast(text: "Something went wrong. Try again later.")
            }
        }
    }
    
    func expressIt(_ row: Int) {
        
        var fe: Feed
        fe = feed[row]
        
        if let vc = storyboard?.instantiateViewController(withIdentifier: "expressVC") as? expressViewController {
            
            vc.feedId = fe.feed_id
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func tapIt(_ row: Int) {
        
        var fe: Feed
        fe = feed[row]
        
        if let popOverVC = storyboard?.instantiateViewController(withIdentifier: "feedEnlargeVC") as? feedPicEnlargedViewController {
            
            let indexPath = IndexPath(row: row, section: 0)
            let cell = feedTable.cellForRow(at: indexPath) as! feedTableViewCell
            
            popOverVC.delegate = self
            popOverVC.feedImage = cell.feedPic.image!
            
            if self.isAdmin() == 0 {
                
                self.setTabBarVisible(visible: !self.tabBarIsVisible(), animated: true)
            }
            popOverVC.feed = fe
            self.addChildViewController(popOverVC)
            popOverVC.view.frame = self.view.frame
            self.view.addSubview(popOverVC.view)
            self.didMove(toParentViewController: self)
        }
    }
}
