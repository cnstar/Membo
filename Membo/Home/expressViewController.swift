//
//  expressViewController.swift
//  Membo
//
//  Created by Ameya Vichare on 24/09/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit

class expressViewController: UIViewController {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var expressTable: UITableView!
    
    var commentTextfield = UITextField()
    var postButton = UIButton()
    var comment = [Comment]()
    var feedId = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.addSpacingToTitle(navBar, title: "EXPRESSIONS")
        makeNavTransparent(navBar)
        setupTable()
        getComments()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.barStyle = .default
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    func getComments() {
        
        self.getServer(url: K.MEMBO_ENDPOINT + "getCommentsListing/\(feedId)/\(self.getUserId())") { [unowned self] (json) in
            
            print(json)
            
            for item in json["comments"].arrayValue {
                
                ImageLoader.sharedLoader.imageForUrl(urlString: item["profilePic"].stringValue, completionHandler: { (image: UIImage?, url: String) in
                    
                    self.comment.append(Comment(userId: item["userId"].stringValue, commentId: item["commentId"].stringValue, message: item["message"].stringValue, status: item["status"].stringValue, profilePic: image!, dateAdded: item["dateAdded"].stringValue, guestName: item["guestName"].stringValue))
                    
                    DispatchQueue.main.async {
                        
                        self.expressTable.reloadData()
                    }
                })
            }
        }
    }
    
    func setupTable() {
        
        expressTable.estimatedRowHeight = 50
        expressTable.rowHeight = UITableViewAutomaticDimension
        expressTable.keyboardDismissMode = .interactive
    }
    
    func makeNavTransparent(_ nav: UINavigationBar) {
        
        nav.setBackgroundImage(UIImage(), for: .default)
        nav.shadowImage = UIImage()
        nav.isTranslucent = true
    }
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    lazy var inputContainerView: UIView = {
        
        let height = self.view.frame.size.height
        let width = self.view.frame.size.width
        
        let accessory = UIView(frame: CGRect(x: 0, y: height - 50, width: width, height: 50))
        accessory.backgroundColor = UIColor.white
        
        let sep = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 0.5))
        sep.backgroundColor = self.UIColorFromHex(rgbValue: 0xdddddd)
        accessory.addSubview(sep)
        
        self.commentTextfield = UITextField(frame: CGRect(x: 20, y: 10, width: width - 80, height: 30))
        self.commentTextfield.font = UIFont(name: "OpenSans-Regular", size: 13.8)
        self.commentTextfield.borderStyle = .none
        self.commentTextfield.placeholder = "You can express here..."
        self.commentTextfield.textColor = UIColor.black
        self.commentTextfield.addTarget(self, action: #selector(self.textfieldValueChanged(_:)), for: .editingChanged)
        accessory.addSubview(self.commentTextfield)
        
        self.postButton = UIButton(frame: CGRect(x: width - 60, y: 5, width: 40, height: 40))
        self.postButton.setImage(#imageLiteral(resourceName: "chatSend"), for: .normal)
        self.postButton.addTarget(self, action: #selector(self.postPressed), for: .touchUpInside)
        self.postButton.alpha = 0.5
        self.postButton.isEnabled = false
        accessory.addSubview(self.postButton)
        
        return accessory
    }()
    
    override var inputAccessoryView: UIView? {
        
        get {
            
            return inputContainerView
        }
    }
    
    override var canBecomeFirstResponder: Bool {
        
        return true
    }
    
    func textfieldValueChanged(_ textField: UITextField) {
        
        if (textField.text?.characters.count)! > 0 {
            
            postButton.alpha = 1
            postButton.isEnabled = true
        }
        else {
            
            postButton.alpha = 0.5
            postButton.isEnabled = false
        }
    }
    
    func postPressed() {
        
        self.comment.append(Comment(userId: "\(self.getUserId())", commentId: "", message: commentTextfield.text!, status: "1", profilePic: self.getUserImage(), dateAdded: "Just now", guestName: self.getUsername()))
        
        DispatchQueue.main.async {
            
            self.expressTable.reloadData()
        }
        
        self.postServer(param: ["user_id": self.getUserId(), "feed_id": feedId, "message": commentTextfield.text!], url: K.MEMBO_ENDPOINT + "postUserCommentPhoto") { [unowned self] (json) in
            
            self.commentTextfield.text?.removeAll()
            
            if json["error"].stringValue == "false" {
                
                
            }
            else if json["error"].stringValue == "true" {
                
                self.Toast(text: "Something went wrong. Try again later.")
            }
        }
    }
}

extension expressViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return comment.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var co: Comment
        co = comment[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "express", for: indexPath) as! expressTableViewCell
        cell.pic.image = co.profilePic
        makeItRound(cell.pic)
        cell.name.text = co.guestName
        cell.comment.text = co.message
        cell.time.text = co.dateAdded
        cell.selectionStyle = .none
        return cell
    }
    
    func makeItRound(_ pic: UIImageView) {
        
        pic.layer.cornerRadius = self.view.frame.size.width * 22.5/414
        pic.clipsToBounds = true
    }
}
